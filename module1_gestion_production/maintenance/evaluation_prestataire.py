# -*- coding: utf-8 -*-

import sys
import openerp

from openerp.osv import fields, osv
from openerp import tools
import datetime
from datetime import date
#from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
import time
from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning









class maintenance_evaluation_prestataire(models.Model):
	_name = 'maintenance.evaluation.prestataire'

	@api.one
	@api.depends('delai_prevu', 'delai_reel')
	def _get_ecart(self):
		if self.delai_prevu and self.delai_reel:
			d_prevu = datetime.datetime.strptime(self.delai_prevu, '%Y-%m-%d')
			d_reel = datetime.datetime.strptime(self.delai_reel, '%Y-%m-%d')

			self.ecart = (d_reel-d_prevu).days

	@api.one
	@api.depends('qualite_service', 'ecart')
	def _get_note(self):
		n = 0
		if self.qualite_service == '2':
			n += 15
		elif self.qualite_service == '1':
			n += 5
		if self.ecart <= 0:
			n += 5
		self.note = n

	@api.one
	@api.depends('note')
	def _get_decision(self):
		if self.note == 20:
			self.decision = 'agree'
		elif self.note == 15:
			self.decision = 'surveiller_delais'
		elif self.note == 10:
			self.decision = 'surveiller_qualite'
		else:
			self.decision = 'eliminer'

	maintenance_prestataire_id = fields.Many2one('maintenance.prestataire', 'Prestataire', ondelete='cascade')
	demande_intervention_id = fields.Many2one('maintenance.demande.intervention', 'Référence', ondelete='cascade')
	qualite_service = fields.Selection([('0', 'Sans impact/Impact négatif(0)'),
										('1', 'Impact médiocre(5)'),
										('2', 'Impact positif(15)'),], string="Qualité de service", default='0')
	delai_prevu = fields.Date('Délai prévu')
	delai_reel = fields.Date('Délai réel')
	ecart = fields.Integer(compute='_get_ecart', string='Ecart(jours)')
	note = fields.Float(compute='_get_note', string='Note/20')
	decision = fields.Selection(compute='_get_decision', 
								selection=[ ('agree', 'Prestataire agrée'),
											('surveiller_qualite', 'Prestataire à surveiller en qualité'),
											('surveiller_delais', 'Prestataire à surveiller en délais'),
											('eliminer', 'Prestataire à éliminer')], 
								string='Décision')