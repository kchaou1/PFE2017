# -*- coding: utf-8 -*-

import sys
import openerp

from openerp.osv import fields, osv
from openerp import tools
import datetime
from datetime import date
#from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
import time
from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning











class maintenance_demande_intervention(models.Model):
	_name = 'maintenance.demande.intervention'
	_rec_name = 'reference_intervention'

	#button workflow annuler
	@api.one
	def action_annuler(self):
		self.write({'state': 'annule'})

	#button workflow planifier
#	@api.one
#	def action_planifier(self):
#		self.write({'state': 'planifie'})

	#button workflow traiter
#	@api.one
#	def action_traiter(self):
#		self.write({'state': 'traite'})

#		#creer demande intervention
#		if self.maintenance_preventive_id and self.maintenance_preventive_id.type_intervalle == 'intervalle': 
#			intervalle = self.maintenance_preventive_id.intervalle
#			date_planifie = datetime.datetime.now() + datetime.timedelta(days=intervalle)
#			self.env['maintenance.demande.intervention'].create({'machine_id': self.machine_id.id,
#																'maintenance_preventive_id': self.maintenance_preventive_id.id,
#																'panne_id': self.panne_id.id,
#																'date_entretien_planifie': date_planifie})

#		if self.maintenance_preventive_id and self.maintenance_preventive_id.type_intervalle == 'duree': 
#			date_planifie = datetime.datetime.now() + datetime.timedelta(days=self.maintenance_preventive_id.duree)
#			duree_planifie = self.maintenance_preventive_id.duree * 7 + self.machine_id.duree_fonctionnement
#			self.env['maintenance.demande.intervention'].create({'machine_id': self.machine_id.id,
#																'maintenance_preventive_id': self.maintenance_preventive_id.id,
#																'panne_id': self.panne_id.id,
#																'date_entretien_planifie': date_planifie,
#																'duree_planifie': duree_planifie})

	#button workflow evaluer
#	@api.one
#	def action_evaluer(self):
#		self.write({'state': 'evalue'})

	#button workflow cloturer
	@api.one
	def action_cloturer(self):
		self.write({'state': 'cloture'})

	@api.one
#	@api.depends('date_entretien_planifie')
	def _get_jours_restants(self):
		if self.date_entretien_planifie:
			dt_p = datetime.datetime.fromtimestamp(time.mktime(time.strptime(self.date_entretien_planifie, "%Y-%m-%d %H:%M:%S")))
			jours_restants = dt_p - datetime.datetime.now()
			self.jours_restants = jours_restants.days

	@api.one
#	@api.depends('duree_planifie')
	def _get_duree_restants(self):
		if self.duree_planifie:
			self.duree_restants = self.duree_planifie - self.machine_id.duree_fonctionnement

	@api.one
	@api.depends('heure_travaillee', 'cout_horaire')
	def _calcul_cout(self):
		for rec in self:
			if rec.cout_horaire and rec.heure_travaillee:
				self.cout = rec.cout_horaire * rec.heure_travaillee

	@api.one
	def _get_delai(self):
		if self.date_entretien_planifie:
			dt_p = datetime.datetime.fromtimestamp(time.mktime(time.strptime(self.date_entretien_planifie, "%Y-%m-%d %H:%M:%S")))
			jours_restants = dt_p - datetime.datetime.now()
			if jours_restants.days > 0 and jours_restants.days <= 10:
				self.delai = 'orange'
			elif jours_restants.days > 10:
				self.delai = 'vert'
			else:
				self.delai = 'rouge'

	reference_intervention = fields.Char('Référence', default='/', required=True)
	#demande
	demandeur = fields.Char('Demandeur')
	maintenance_preventive_id = fields.Many2one('maintenance.preventive', 'Référence MP', ondelete='cascade')
	type_intervalle_rel = fields.Selection([('duree','Durée de fonctionnement'),
											('intervalle','Intervalle')], string='Type',
											related='maintenance_preventive_id.type_intervalle')	
	date_demande = fields.Datetime('Date demande', default= lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'))
	machine_id = fields.Many2one('production.machine', 'Machine', required=True, ondelete='cascade')
	panne_id = fields.Many2one('maintenance.panne', 'Panne?', required=True, ondelete='cascade')
	priorite = fields.Selection([('basse','Basse'),
								 ('normal','Normal'),
								 ('urgent','Urgent'),
								 ('autres','Autres')], 'Priorité', default='normal')
	jours_restants = fields.Integer(compute='_get_jours_restants', string='Delai (j)')
	duree_restants = fields.Integer(compute='_get_duree_restants', string='Durée restants (h)')

	#planification
	type_prestataire = fields.Selection([('externe','Externe'),
										 ('interne','Interne')], 'Type prestataire', default='externe')
	prestataire_id = fields.Many2one('maintenance.prestataire', 'Prestataire', ondelete='cascade')
	operateur_id = fields.Many2one('production.operateur', 'Operateur', ondelete='cascade')
	date_entretien_planifie = fields.Datetime('Date entretien planifiée')
	duree_planifie = fields.Float('Durée de fonctionnement planifiée')
	#traitement
	traitement_effectue = fields.Text('Traitement effectué')
	demande_intervention_piece_rel_ids = fields.One2many('demande.intervention.piece.rel', 'demande_intervention_id', 'Pièces de rechange')
	date_entretien = fields.Datetime('Date début entretien')
	date_fin_entretien = fields.Datetime('Date fin entretien')

	heure_travaillee = fields.Integer('Heure travaillée')
	cout_horaire = fields.Float('Coût horaire', related="operateur_id.cout_horaire")
	cout = fields.Float(compute='_calcul_cout', string='Coût')
	montant_facture = fields.Float('Montant facturé')
	temp_arret = fields.Float('temp d\'arrêt (h)')

	#evaluation
	date_evaluation = fields.Date('Date évaluation')

	remarque = fields.Text('Remarque')
	state = fields.Selection([('non_planifie', 'Non planifié'),
							  ('planifie', 'Planifiée'),
							  ('traite', 'Traitée'),
							  ('evalue', 'Evaluée'),
							  ('cloture', 'Cloturée'),
							  ('annule', 'Annulée')], 'Etat', required=True, default='non_planifie')

	delai = fields.Selection([('vert','vert'),
							('rouge','rouge'),
							('orange','orange')], compute='_get_delai', string='Delai')

	@api.model
	def create(self, values):
		#generer code sequence "reference_intervention" s'il n'est pas spécifié
		if ('reference_intervention' not in values) or (values.get('reference_intervention')=='/'):
			values['reference_intervention'] = self.env['ir.sequence'].get('maintenance.demande.intervention')

		#test reference_intervention doit etre unique
		if self.env['maintenance.demande.intervention'].search_count([('reference_intervention', '=', values['reference_intervention'])]) > 0:
			raise Warning(_('Erreur!'), 
						_('Référence intervention existe déjà [ %s ].')% (values['reference_intervention']))

		return super(maintenance_demande_intervention, self).create(values)

	@api.multi
	def write(self, values):
		obj_id=super(maintenance_demande_intervention, self).write(values)
		#test reference_intervention doit etre unique
		if self.env['maintenance.demande.intervention'].search_count([('reference_intervention', '=', self.reference_intervention)]) > 1:
			raise Warning(_('Erreur!'), 
						_('Référence intervention existe déjà [ %s ].')% (self.reference_intervention))

		return obj_id

	@api.multi
	def evaluer_prestataire(self):
		self.date_evaluation = datetime.date.today()
		self.state = 'evalue'
		return { 
				'name': _("Evaluation"),
				'type': 'ir.actions.act_window',
				'view_type': 'form',
				'view_mode': 'form',
				'res_model': 'maintenance.evaluation.prestataire',
				'view_id': False,
				'target': 'new',
				'context': {'default_maintenance_prestataire_id': self.prestataire_id.id,
							'default_demande_intervention_id': self.id,
							'default_delai_prevu': self.date_entretien_planifie,
							'default_delai_reel': self.date_entretien,
							'default_maintenance_corrective_id': self.id,
							},
				}

	@api.multi
	def evaluer_operateur(self):
		self.date_evaluation = datetime.date.today()
		self.state = 'evalue'
		return { 
				'name': _("Evaluation"),
				'type': 'ir.actions.act_window',
				'view_type': 'form',
				'view_mode': 'form',
				'res_model': 'maintenance.evaluation.operateur',
				'view_id': False,
				'target': 'new',
				'context': {'default_production_operateur_id': self.operateur_id.id,
							'default_date_evaluation': self.date_evaluation,
							},
				}




class maintenance_demande_intervention(models.Model):
	_name = 'maintenance.demande.intervention'
	_rec_name = 'reference_intervention'

	#button workflow annuler
	@api.one
	def action_annuler(self):
		self.write({'state': 'annule'})

	#button workflow planifier
#	@api.one
#	def action_planifier(self):
#		self.write({'state': 'planifie'})

	#button workflow traiter
#	@api.one
#	def action_traiter(self):
#		self.write({'state': 'traite'})

#		#creer demande intervention
#		if self.maintenance_preventive_id and self.maintenance_preventive_id.type_intervalle == 'intervalle': 
#			intervalle = self.maintenance_preventive_id.intervalle
#			date_planifie = datetime.datetime.now() + datetime.timedelta(days=intervalle)
#			self.env['maintenance.demande.intervention'].create({'machine_id': self.machine_id.id,
#																'maintenance_preventive_id': self.maintenance_preventive_id.id,
#																'panne_id': self.panne_id.id,
#																'date_entretien_planifie': date_planifie})

#		if self.maintenance_preventive_id and self.maintenance_preventive_id.type_intervalle == 'duree': 
#			date_planifie = datetime.datetime.now() + datetime.timedelta(days=self.maintenance_preventive_id.duree)
#			duree_planifie = self.maintenance_preventive_id.duree * 7 + self.machine_id.duree_fonctionnement
#			self.env['maintenance.demande.intervention'].create({'machine_id': self.machine_id.id,
#																'maintenance_preventive_id': self.maintenance_preventive_id.id,
#																'panne_id': self.panne_id.id,
#																'date_entretien_planifie': date_planifie,
#																'duree_planifie': duree_planifie})

	#button workflow evaluer
#	@api.one
#	def action_evaluer(self):
#		self.write({'state': 'evalue'})

	#button workflow cloturer
	@api.one
	def action_cloturer(self):
		self.write({'state': 'cloture'})

	@api.one
#	@api.depends('date_entretien_planifie')
	def _get_jours_restants(self):
		if self.date_entretien_planifie:
			dt_p = datetime.datetime.fromtimestamp(time.mktime(time.strptime(self.date_entretien_planifie, "%Y-%m-%d %H:%M:%S")))
			jours_restants = dt_p - datetime.datetime.now()
			self.jours_restants = jours_restants.days

	@api.one
#	@api.depends('duree_planifie')
	def _get_duree_restants(self):
		if self.duree_planifie:
			self.duree_restants = self.duree_planifie - self.machine_id.duree_fonctionnement

	@api.one
	@api.depends('heure_travaillee', 'cout_horaire')
	def _calcul_cout(self):
		for rec in self:
			if rec.cout_horaire and rec.heure_travaillee:
				self.cout = rec.cout_horaire * rec.heure_travaillee

	@api.one
	def _get_delai(self):
		if self.date_entretien_planifie:
			dt_p = datetime.datetime.fromtimestamp(time.mktime(time.strptime(self.date_entretien_planifie, "%Y-%m-%d %H:%M:%S")))
			jours_restants = dt_p - datetime.datetime.now()
			if jours_restants.days > 0 and jours_restants.days <= 10:
				self.delai = 'orange'
			elif jours_restants.days > 10:
				self.delai = 'vert'
			else:
				self.delai = 'rouge'

	reference_intervention = fields.Char('Référence', default='/', required=True)
	#demande
	demandeur = fields.Char('Demandeur')
	maintenance_preventive_id = fields.Many2one('maintenance.preventive', 'Référence MP', ondelete='cascade')
	type_intervalle_rel = fields.Selection([('duree','Durée de fonctionnement'),
											('intervalle','Intervalle')], string='Type',
											related='maintenance_preventive_id.type_intervalle')	
	date_demande = fields.Datetime('Date demande', default= lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'))
	machine_id = fields.Many2one('production.machine', 'Machine', required=True, ondelete='cascade')
	panne_id = fields.Many2one('maintenance.panne', 'Panne?', required=True, ondelete='cascade')
	priorite = fields.Selection([('basse','Basse'),
								 ('normal','Normal'),
								 ('urgent','Urgent'),
								 ('autres','Autres')], 'Priorité', default='normal')
	jours_restants = fields.Integer(compute='_get_jours_restants', string='Delai (j)')
	duree_restants = fields.Integer(compute='_get_duree_restants', string='Durée restants (h)')

	#planification
	type_prestataire = fields.Selection([('externe','Externe'),
										 ('interne','Interne')], 'Type prestataire', default='externe')
	prestataire_id = fields.Many2one('maintenance.prestataire', 'Prestataire', ondelete='cascade')
	operateur_id = fields.Many2one('production.operateur', 'Operateur', ondelete='cascade')
	date_entretien_planifie = fields.Datetime('Date entretien planifiée')
	duree_planifie = fields.Float('Durée de fonctionnement planifiée')
	#traitement
	traitement_effectue = fields.Text('Traitement effectué')
	demande_intervention_piece_rel_ids = fields.One2many('demande.intervention.piece.rel', 'demande_intervention_id', 'Pièces de rechange')
	date_entretien = fields.Datetime('Date début entretien')
	date_fin_entretien = fields.Datetime('Date fin entretien')

	heure_travaillee = fields.Integer('Heure travaillée')
	cout_horaire = fields.Float('Coût horaire', related="operateur_id.cout_horaire")
	cout = fields.Float(compute='_calcul_cout', string='Coût')
	montant_facture = fields.Float('Montant facturé')
	temp_arret = fields.Float('temp d\'arrêt (h)')

	#evaluation
	date_evaluation = fields.Date('Date évaluation')

	remarque = fields.Text('Remarque')
	state = fields.Selection([('non_planifie', 'Non planifié'),
							  ('planifie', 'Planifiée'),
							  ('traite', 'Traitée'),
							  ('evalue', 'Evaluée'),
							  ('cloture', 'Cloturée'),
							  ('annule', 'Annulée')], 'Etat', required=True, default='non_planifie')

	delai = fields.Selection([('vert','vert'),
							('rouge','rouge'),
							('orange','orange')], compute='_get_delai', string='Delai')

	@api.model
	def create(self, values):
		#generer code sequence "reference_intervention" s'il n'est pas spécifié
		if ('reference_intervention' not in values) or (values.get('reference_intervention')=='/'):
			values['reference_intervention'] = self.env['ir.sequence'].get('maintenance.demande.intervention')

		#test reference_intervention doit etre unique
		if self.env['maintenance.demande.intervention'].search_count([('reference_intervention', '=', values['reference_intervention'])]) > 0:
			raise Warning(_('Erreur!'), 
						_('Référence intervention existe déjà [ %s ].')% (values['reference_intervention']))

		return super(maintenance_demande_intervention, self).create(values)

	@api.multi
	def write(self, values):
		obj_id=super(maintenance_demande_intervention, self).write(values)
		#test reference_intervention doit etre unique
		if self.env['maintenance.demande.intervention'].search_count([('reference_intervention', '=', self.reference_intervention)]) > 1:
			raise Warning(_('Erreur!'), 
						_('Référence intervention existe déjà [ %s ].')% (self.reference_intervention))

		return obj_id

	@api.multi
	def evaluer_prestataire(self):
		self.date_evaluation = datetime.date.today()
		self.state = 'evalue'
		return { 
				'name': _("Evaluation"),
				'type': 'ir.actions.act_window',
				'view_type': 'form',
				'view_mode': 'form',
				'res_model': 'maintenance.evaluation.prestataire',
				'view_id': False,
				'target': 'new',
				'context': {'default_maintenance_prestataire_id': self.prestataire_id.id,
							'default_demande_intervention_id': self.id,
							'default_delai_prevu': self.date_entretien_planifie,
							'default_delai_reel': self.date_entretien,
							'default_maintenance_corrective_id': self.id,
							},
				}

	@api.multi
	def evaluer_operateur(self):
		self.date_evaluation = datetime.date.today()
		self.state = 'evalue'
		return { 
				'name': _("Evaluation"),
				'type': 'ir.actions.act_window',
				'view_type': 'form',
				'view_mode': 'form',
				'res_model': 'maintenance.evaluation.operateur',
				'view_id': False,
				'target': 'new',
				'context': {'default_production_operateur_id': self.operateur_id.id,
							'default_date_evaluation': self.date_evaluation,
							},
				}
