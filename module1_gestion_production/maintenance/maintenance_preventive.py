# -*- coding: utf-8 -*-

import sys
import openerp

from openerp.osv import fields, osv
from openerp import tools
import datetime
from datetime import date
#from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
import time
from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning







class maintenance_preventive(models.Model):
	_name = 'maintenance.preventive'
	_rec_name = 'reference_maintenance_preventive'

	reference_maintenance_preventive = fields.Char('Référence', default='/', required=True)
	machine_id = fields.Many2one('production.machine', 'Machine', required=True, ondelete='cascade')
	type_intervalle = fields.Selection([('duree','Durée de fonctionnement'),
										('intervalle','Intervalle')], string='Type', default='intervalle', required=True)
	intervalle = fields.Integer('Intervalle (en jours)')
	duree = fields.Integer('Durée (en jours)')
	commence_le = fields.Date('Commencé le', default= lambda *a:datetime.datetime.now().strftime('%Y-%m-%d'), required=True)
	panne_id = fields.Many2one('maintenance.panne', 'Intervention', required=True, ondelete='cascade')
	demande_intervention_ids = fields.One2many('maintenance.demande.intervention', 'maintenance_preventive_id', 
												'Demandes d\'intervention')

	@api.model
	def create(self, values):
		#test intervalle et duree doivent etre > 0
		if values['type_intervalle'] == 'intervalle' and values['intervalle'] <= 0:
			raise Warning(_('Erreur!'), 
						_('Il faut que :  intervalle > 0'))
		if values['type_intervalle'] == 'duree' and values['duree'] <= 0:
			raise Warning(_('Erreur!'), 
						_('Il faut que :  duree > 0'))

		#generer code sequence "reference_maintenance_preventive" s'il n'est pas spécifié
		if ('reference_maintenance_preventive' not in values) or (values.get('reference_maintenance_preventive')=='/'):
			values['reference_maintenance_preventive'] = self.env['ir.sequence'].get('maintenance.preventive')

		#test reference_maintenance_preventive doit etre unique
		if self.env['maintenance.preventive'].search_count([('reference_maintenance_preventive', '=', values['reference_maintenance_preventive'])]) > 0:
			raise Warning(_('Erreur!'), 
						_('Référence maintenance préventive existe déjà [ %s ].')% (values['reference_maintenance_preventive']))

		new_id = super(maintenance_preventive, self).create(values)

		#creer demande intervention
		if values['type_intervalle'] == 'intervalle':
			#date_planifie = datetime.datetime.now() + datetime.timedelta(days=values['intervalle'])
			date_planifie = datetime.datetime.strptime(values['commence_le'], '%Y-%m-%d') + datetime.timedelta(days=values['intervalle'])
			self.env['maintenance.demande.intervention'].create({'machine_id': values['machine_id'],
																'maintenance_preventive_id': new_id.id,
																'panne_id': values['panne_id'],
																'date_entretien_planifie': date_planifie,
																'state': 'planifie'})
		if values['type_intervalle'] == 'duree':
			m_obj = self.env['production.machine'].browse(values['machine_id'])
			#date_planifie = datetime.datetime.now() + datetime.timedelta(days=values['duree'])
			date_planifie = datetime.datetime.strptime(values['commence_le'], '%Y-%m-%d') + datetime.timedelta(days=values['duree'])
			duree_planifie = m_obj.duree_fonctionnement + values['duree'] * 7
			self.env['maintenance.demande.intervention'].create({'machine_id': values['machine_id'],
																'maintenance_preventive_id': new_id.id,
																'panne_id': values['panne_id'],
																'date_entretien_planifie': date_planifie,
																'state': 'planifie',
																'duree_planifie': duree_planifie})

		return new_id

	@api.multi
	def write(self, values):
		obj_id=super(maintenance_preventive, self).write(values)
		for obj in self:
			#test intervalle et duree doivent etre > 0
			if obj.type_intervalle == 'intervalle' and obj.intervalle <= 0:
				raise Warning(_('Erreur!'), 
						_('Il faut que :  intervalle > 0'))
			if obj.type_intervalle == 'duree' and obj.duree <= 0:
				raise Warning(_('Erreur!'), 
						_('Il faut que :  duree > 0'))


		#test reference_maintenance_preventive doit etre unique
		if self.env['maintenance.preventive'].search_count([('reference_maintenance_preventive', '=', self.reference_maintenance_preventive)]) > 1:
			raise Warning(_('Erreur!'), 
						_('Référence maintenance préventive existe déjà [ %s ].')% (self.reference_maintenance_preventive))

		return obj_id