# -*- coding: utf-8 -*-

import sys
import openerp

from openerp.osv import fields, osv
from openerp import tools
import datetime
from datetime import date
#from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
import time
from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning






class maintenance_prestataire(models.Model):

	@api.multi
	@api.depends('name', 'note_moyenne')
	def name_get(self):
		result = []
		for record in self:
			note_moyenne = str(record.note_moyenne) if record.note_moyenne != False else ''
			result.append((record.id, record.name + ' ( ' + note_moyenne + ' /20)')) 
		return result

	_name = 'maintenance.prestataire'

	@api.one
	def _get_note_moyenne(self):
		total = 0.0
		con = 0.0
		for m in self:
			for e in m.evaluation_ids:
				total += e.note
				con += 1

		if con > 0:
			self.note_moyenne = total / con

	name = fields.Char('Nom', required=True)
	evaluation_ids = fields.One2many('maintenance.evaluation.prestataire', 'maintenance_prestataire_id', 'Evaluations')
	note_moyenne = fields.Float(compute='_get_note_moyenne', string="Note moyenne")