# -*- coding: utf-8 -*-

import sys
import openerp

from openerp.osv import fields, osv
from openerp import tools
import datetime
from datetime import date
#from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
import time
from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning










class bon_entree_piece(models.Model):
	_name='bon.entree.piece'

	code_bon = fields.Char('Code bon :', readonly=True)
	piece_id = fields.Many2one('maintenance.piece', 'Pièce de rechange', ondelete='cascade', required=True)
	quantite = fields.Float('Quantité', required=True)
	unite = fields.Selection([('kg','kg'),
							  ('U','U'),
							  ('m','m'),
							  ('m2','m²'),
							  ('m3','m³'),
							  ('l','l'),], related='piece_id.unite', readonly=True)
	date = fields.Date('Date', default= lambda *a:datetime.datetime.now().strftime('%Y-%m-%d'), required=True)
	fournisseur_id = fields.Many2one('achat.fournisseur', 'Fournisseur', ondelete='cascade')

	@api.model
	def create(self, values):
		#test si quantite <= 0 on genere exception
		if values['quantite'] <= 0:
			raise Warning(_('Erreur!'), 
						_('La quantité doit étre supérieur strictement à zero ( %s )')% (values['quantite']))

		#generer code sequence "code_bon"
		values['code_bon'] = self.env['ir.sequence'].get('bon.entree.piece')

		#augmenter stock
		for piece in self.env['maintenance.piece'].browse(values['piece_id']):
			piece.quantite_stock += values['quantite']

		new_id = super(bon_entree_piece, self).create(values)
		return new_id

	@api.multi
	def write(self, values):
		nouv_quantite = values.get('quantite', None)
		nouv_piece_id = values.get('piece_id', None)
		ancien_piece_obj = self.env['maintenance.piece'].browse(self.piece_id.id)

		#test si quantite <= 0 on genere exception
		if nouv_quantite:
			if nouv_quantite <= 0:
				raise Warning(_('Erreur!'), 
							_('La quantité doit étre supérieur strictement à zero ( %s )')% (nouv_quantite))

		#modifier stock
		if nouv_piece_id:
			nouv_piece_obj = self.env['maintenance.piece'].browse(nouv_piece_id)
			if nouv_quantite:
				ancien_piece_obj.quantite_stock -= self.quantite
				nouv_piece_obj.quantite_stock += nouv_quantite
			else:#quantite non changer
				ancien_piece_obj.quantite_stock -= self.quantite
				nouv_piece_obj.quantite_stock += self.quantite
		else:#piece non changer
			if nouv_quantite:
				ancien_piece_obj.quantite_stock -= self.quantite
				ancien_piece_obj.quantite_stock += nouv_quantite

		obj_id=super(bon_entree_piece, self).write(values)

		return obj_id

	@api.multi
	def unlink(self):
		for rec in self:
			#retirer la quantite du stock
			piece_obj = self.env['maintenance.piece'].browse(rec.piece_id.id)
			piece_obj.quantite_stock -= rec.quantite

		return super(bon_entree_piece, self).unlink()