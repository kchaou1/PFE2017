# -*- coding: utf-8 -*-

import sys
import openerp

from openerp.osv import fields, osv
from openerp import tools
import datetime
from datetime import date
#from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
import time
from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning



class maintenance_piece(models.Model):
	_name = 'maintenance.piece'

	#code : nom
	@api.multi
	@api.depends('code_piece', 'name')
	def name_get(self):
		result = []
		for record in self:
			result.append((record.id, record.code_piece + ' : ' + record.name))
		return result

	@api.one
	def _get_bon_entree_piece_count(self):
		self.bon_entree_piece_count = len(self.bon_entree_piece_ids)

	code_piece = fields.Char('Code', required=True)
	name = fields.Char('Nom', required=True)
	quantite_stock = fields.Float('Quantite stock')  # name, digits
	stock_min = fields.Float('Stock minimum')
	unite = fields.Selection([('kg','kg'),
							  ('U','U'),
							  ('m','m'),
							  ('m2','m²'),
							  ('m3','m³'),
							  ('l','l'),], 'Unité')
	prix = fields.Float('Prix unitaire (DT)')
	bon_entree_piece_ids = fields.One2many('bon.entree.piece', 'piece_id', 'Bon entrée pièce de rechange')
	bon_entree_piece_count = fields.Integer(compute='_get_bon_entree_piece_count', string='B.E. Pièce')


	@api.model
	def create(self, values):
		#test code_piece doit etre unique
		if self.env['maintenance.piece'].search_count([('code_piece', '=', values['code_piece'])]) > 0:
			raise Warning(_('Erreur!'), 
						_('Code piece existe déjà [ %s ].')% (values['code_piece']))

		new_id = super(maintenance_piece, self).create(values)
		return new_id


	@api.multi
	def write(self, values):
		obj_id = super(maintenance_piece, self).write(values)
		#test code_piece doit etre unique
		if self.env['maintenance.piece'].search_count([('code_piece', '=', self.code_piece)]) > 1:
			raise Warning(_('Erreur!'),
						_('Code piece existe déjà [ %s ].')% (self.code_piece))

		return obj_id


	@api.multi
	def ajouter_bon_entree_piece(self):
		return { 
				'name': _("Bon entrée pièce de rechange"),
				'type': 'ir.actions.act_window',
				'view_type': 'form',
				'view_mode': 'form',
				'res_model': 'bon.entree.piece',
				'view_id': False,
				'context': {'default_piece_id': self.id},
				}

	def notif(self, title, record_name, res_id, model, recepteur):
		mail_vals = {
					'body': '<html>'+title+'</html>',
					'record_name': record_name,
					'res_id': res_id,
					'reply_to': self.env['res.users'].browse(self.env.uid).name,
					'author_id': self.env['res.users'].browse(self.env.uid).partner_id.id,
					'model': model,
					'type': 'email',
					'email_from': self.env['res.users'].browse(self.env.uid).name,
					'starred': True,
					}
		message = self.env['mail.message'].create(mail_vals)

		mail_notif_vals = {
						'partner_id': self.env['res.users'].browse(recepteur).partner_id.id,
						'message_id': message.id,
						'is_read': False,
						'starred': True,
						}
		self.env['mail.notification'].create(mail_notif_vals)

		return True

	@api.model
	def verifier_stock_piece(self):
		group = self.env['res.groups'].search([('name', '=', 'Groupe production')])
		for piece in self.env['maintenance.piece'].search([]):
			if piece.quantite_stock <= piece.stock_min:
				for user in group.users:
					title = 'Stock piéce de rechange insuffisant'
					record_name = piece.code_piece
					res_id = piece.id
					model = 'maintenance.piece'
					recepteur = user.id
					self.notif(title, record_name, res_id, model, recepteur)

class maintenance_piece_mail(models.Model):
	_name = "maintenance.piece"
	_inherit = ['maintenance.piece','mail.thread']






