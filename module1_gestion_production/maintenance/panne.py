# -*- coding: utf-8 -*-

import sys
import openerp

from openerp.osv import fields, osv
from openerp import tools
import datetime
from datetime import date
#from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
import time
from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning


class maintenance_panne(models.Model):
	_name = 'maintenance.panne'
	_order = "occurrence_panne"

	@api.one
	@api.depends('demande_intervention_ids')
	def _get_occurrence_panne(self):
		self.occurrence_panne = len(self.demande_intervention_ids)

	@api.one
	@api.depends('demande_intervention_ids')
	def _get_panne_courante(self):
		for p in self.env['maintenance.panne'].search([]):
			p.panne_courante = False
		for p in self.env['maintenance.panne'].search([], limit=5, order='occurrence_panne desc'):
			p.panne_courante = True

	name = fields.Char('Panne', required=True)
	description = fields.Text('Description')
	occurrence_panne = fields.Integer(compute='_get_occurrence_panne', string="Nombre d\'occurrence", store=True)
	demande_intervention_ids = fields.One2many('maintenance.demande.intervention', 'panne_id', 'Demande intervention')
	panne_courante = fields.Boolean(compute='_get_panne_courante', string='Panne courante', store=True)