# -*- coding: utf-8 -*-

import sys
import openerp

from openerp.osv import fields, osv
from openerp import tools
import datetime
from datetime import date
#from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
import time
from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning


class maintenance_evaluation_operateur(models.Model):
	_name = 'maintenance.evaluation.operateur'

	production_operateur_id = fields.Many2one('production.operateur', 'Operateur', ondelete='cascade')
	qualite_service = fields.Selection([('0', '0/20'),
										('1', '5/20'),
										('2', '10/20'),
										('3', '15/20'),
										('4', '20/20'),], string="Efficacité (/20)", default='0')
	date_evaluation = fields.Date('Date évaluation')