#-*- coding: utf-8 -*-

import sys
import openerp

from openerp.osv import fields, osv
from openerp import tools
import datetime
from datetime import date
#from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
import time
from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning









class demande_intervention_piece_rel(models.Model):
	_name = 'demande.intervention.piece.rel'

	demande_intervention_id = fields.Many2one('maintenance.demande.intervention', 'Demande intervention', ondelete='cascade')
	piece_id = fields.Many2one('maintenance.piece', 'Pièce', required=True, ondelete='cascade')
	quantite = fields.Float('Quantité', required=True)
	unite = fields.Selection([('kg','kg'),
							  ('U','U'),
							  ('m','m'),
							  ('m2','m²'),
							  ('m3','m³'),
							  ('l','l'),], related='piece_id.unite', readonly=True)
	prix = fields.Float('Prix unitaire (DT)', related='piece_id.prix', readonly=True)
	date = fields.Date('Date', default= lambda *a:datetime.datetime.now().strftime('%Y-%m-%d'), required=True)

	@api.model
	def create(self, values):
		#test quantite <= 0
		if values['quantite'] <= 0:
			raise Warning(_('Erreur!'), 
						_('La quantité piéce doit être positive'))

		#retirer du stock la quantite des pieces utilisé
		for piece in self.env['maintenance.piece'].browse(values['piece_id']):
			piece.quantite_stock -= values['quantite']

		return super(demande_intervention_piece_rel, self).create(values)

	@api.multi
	def write(self, values):
		nouv_quantite = values.get('quantite', None)
		nouv_piece_id = values.get('piece_id', None)
		ancien_piece_obj = self.env['maintenance.piece'].browse(self.piece_id.id)


		#test si quantite <= 0 on genere exception
		if nouv_quantite:
			if nouv_quantite <= 0:
				raise Warning(_('Erreur!'), 
							_('La quantité doit étre supérieur strictement à zero ( %s )')% (nouv_quantite))

		#modifier stock
		if nouv_piece_id:
			nouv_piece_obj = self.env['maintenance.piece'].browse(nouv_piece_id)
			if nouv_quantite:
				ancien_piece_obj.quantite_stock += self.quantite
				nouv_piece_obj.quantite_stock -= nouv_quantite
			else:#quantite non changer
				ancien_piece_obj.quantite_stock += self.quantite
				nouv_piece_obj.quantite_stock -= self.quantite
		else:#piece non changer
			if nouv_quantite:
				ancien_piece_obj.quantite_stock += self.quantite
				ancien_piece_obj.quantite_stock -= nouv_quantite

		obj_id=super(demande_intervention_piece_rel, self).write(values)

		return obj_id

	@api.multi
	def unlink(self):
		for rec in self:
			#ajouter la quantite du stock retirer
			piece_obj = self.env['maintenance.piece'].browse(rec.piece_id.id)
			piece_obj.quantite_stock += rec.quantite

		return super(demande_intervention_piece_rel, self).unlink()








