
Module 1:gestion de production et maintenance

Description:Le module production contient quatre sous modules . chaque sous-module est représenté par un fichier controlleur(.py) qui contient les attributs et les fonctions et un fichier vue(.xml) pour representation des données.

Architecture:

1)la gestion de production    (production.py,production_view.xml)

2)la gestion de maintenance   (maintenance.py,maintenance_view.xml)

3)la gestion d'achat    (achat.py,achat_view.xml)

4)la gestion de vente    (vente.py,vente_view.xml)
