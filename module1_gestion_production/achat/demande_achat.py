# -*- coding: utf-8 -*-

import sys
import openerp
from openerp import models, fields, api, _
from openerp import tools
from datetime import date
from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning



class demande_achat(models.Model):

	_name = 'demande.achat'

	date_demande = fields.Date('Date demande', default= lambda *a:datetime.now().strftime('%Y-%m-%d'))
	demandeur = fields.Many2one('res.users', 'Demandeur', default= lambda self: self.env.user)
	article_id = fields.Many2one('production.article', 'Article', ondelete='cascade', required=True)
	quantite_demande = fields.Float('Quantité demandé', required=True)