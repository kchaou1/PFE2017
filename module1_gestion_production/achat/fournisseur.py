# -*- coding: utf-8 -*-

import sys
import openerp
from openerp import models, fields, api, _
from openerp import tools
from datetime import date
from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning

class commande_fournisseur(models.Model):

	@api.one
	@api.depends('state')
	def _check_color(self):
		for rec in self:
			color = 0
			color_value = self.env['color.status'].search([('state', '=', rec.state)], limit=1).color
			if color_value:
				color = color_value

			self.member_color = color

	#button workflow Envoyer
	@api.one
	def action_envoyer_commande(self):
		if self.article_commande_fournisseur_ids:
			self.write({'state': 'attente'})
		else:
			raise Warning(_('Erreur!'), 
						_("Cette commande (%s) ne contient aucun article") % self.code_commande)

	#button workflow Cloturer
	@api.one
	def action_cloturer_commande(self):
		total = True
		for line in self.article_commande_fournisseur_ids:
			if line.quantite_livre < line.quantite:
				total = False
				break
		if total == True:
			self.write({'state': 'recu_total'})
		else:
			self.write({'state': 'recu_partiel'})

	#button workflow Annuler
	@api.one
	def action_annuler_commande(self):
		self.write({'state': 'annulee'})

	_name = 'commande.fournisseur'
	_rec_name = 'code_commande'

	member_color = fields.Integer(compute='_check_color', string='Color')
	code_commande = fields.Char('Code commande :', required=True)
	fournisseur_id = fields.Many2one('achat.fournisseur', 'Fournisseur', required=True, ondelete='cascade')
	date_commande = fields.Date('Date de commande', required=True, default= lambda *a:datetime.now().strftime('%Y-%m-%d'))
	article_commande_fournisseur_ids = fields.One2many('article.commande.fournisseur.rel', 'commande_fournisseur_id', 'Articles')
	bon_entree_achat_ids = fields.One2many('bon.entree.achat', 'commande_id', 'Bons entrées achat')
	state = fields.Selection([('brouillon','Brouillon'),
							  ('annulee','Annulée'),
							  ('attente','En attente de réception'),
							  ('recu_partiel','Reçu partiellemet'),
							  ('recu_total','Reçu totalemet'),], 'Etat', readonly=True, default='brouillon')

	@api.model
	def create(self, values):
		#test code_commande doit etre unique
		if self.env['commande.fournisseur'].search_count([('code_commande', '=', values['code_commande'])]) > 0:
			raise Warning(_('Erreur!'),
						_('Code commande existe déjà [ %s ].')% (values['code_commande']))

		#test si le même article ajouté plusieur fois on génére exception
		ids = []
		articles = values.get("article_commande_fournisseur_ids", None)
		for article in articles:
			article_id = article[2].get("article_id", None)
			if article_id and article_id in ids:
				artcile_obj = self.env['production.article'].browse(article_id)
				raise Warning(_('Erreur!'), 
							_("Même article ajouté plusieurs fois : %s") % artcile_obj.code_article)
			ids.append(article_id)

		new_id = super(commande_fournisseur, self).create(values)
		return new_id

	@api.multi
	def write(self, values):
		obj_id = super(commande_fournisseur, self).write(values)
		#test code_commande doit etre unique
		if self.env['commande.fournisseur'].search_count([('code_commande', '=', self.code_commande)]) > 1:
			raise Warning(_('Erreur!'),
						_('Code commande existe déjà [ %s ].')% (self.code_commande))

		#test si le même article ajouté plusieur fois on génére exception
		ids = []
		for line in self.article_commande_fournisseur_ids:
			if line.article_id.id in ids:
				raise Warning(_('Erreur!'), 
							_("Même article ajouté plusieurs fois : %s") % line.article_id.code_article)
			ids.append(line.article_id.id)
	    
		return obj_id