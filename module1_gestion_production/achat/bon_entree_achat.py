# -*- coding: utf-8 -*-

import sys
import openerp
from openerp import models, fields, api, _
from openerp import tools
from datetime import date
from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning



class bon_entree_achat(models.Model):

	@api.one
	@api.depends('article_id', 'commande_id')
	def _get_quantite_commande(self):
		if self.article_id and self.commande_id:
			self.quantite_commande = self.env['article.commande.fournisseur.rel'].search([
																	('article_id', '=', self.article_id.id), 
																  	('commande_fournisseur_id', '=', self.commande_id.id)], 
																	limit=1).quantite

	@api.one
	@api.depends('article_id', 'commande_id')
	def _get_quantite_livre(self):
		qte = 0
		if self.commande_id and self.article_id:
			bon_entree_achat_ids = self.env['bon.entree.achat'].search([('commande_id', '=', self.commande_id.id), 
																		('article_id', '=', self.article_id.id)])
			for bea in bon_entree_achat_ids:
				qte += bea.quantite

		self.quantite_livre = qte

	@api.one
	@api.depends('quantite_commande', 'quantite_livre')
	def _get_progress(self):
		if self.quantite_commande > 0 and self.quantite_livre > 0:
			self.progress = self.quantite_livre / self.quantite_commande * 100
		else:
			self.progress = 0

	_name = 'bon.entree.achat'
	_rec_name = 'code_bon'

	code_bon = fields.Char('Code bon :', readonly=True)
	date_bon = fields.Date('Date', required=True, default= lambda *a:datetime.now().strftime('%Y-%m-%d'))
	commande_id = fields.Many2one('commande.fournisseur', 'Commande', required=True, ondelete='cascade', 
									domain="[('state', '=', 'attente')]" )
	article_id = fields.Many2one('production.article', 'Article', required=True, ondelete='cascade')
	quantite = fields.Float('Quantité',required=True)
	unite = fields.Selection([('u','U'),
				   			  ('kg','Kg'),
				   			  ('m2','m²'),
				   			  ('m','m')], related='article_id.unite', readonly=True, string='Unite')
	quantite_commande = fields.Float(compute='_get_quantite_commande', string='Quantité commandé')
	quantite_livre = fields.Float(compute='_get_quantite_livre', string='Quantité livré')
	progress = fields.Float(compute='_get_progress', string='Progression')

	@api.model
	def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
		res = super(bon_entree_achat, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
		for field in res['fields']:
			if field == 'article_id':
				res['fields'][field]['domain'] = [('id','in', [])]

		return res

	@api.model
	def create(self, values):
		#test si quantite <= 0 on genere exception
		if values['quantite'] <= 0:
			raise Warning(_('Erreur!'), 
						_("La quantité doit étre supérieur strictement à zero"))

		#generer code sequence "code_bon"
		values['code_bon'] = self.env['ir.sequence'].get('bon.entree.achat')

		#augmenter stock
		for article in self.env['production.article'].browse(values['article_id']):
			article.stock_reel += values['quantite']

			#test stock maximale
			article.verifier_stock()

		new_id = super(bon_entree_achat, self).create(values)
		return new_id

	@api.multi
	def write(self, values):

		nouv_article = values.get('article_id', None)
		nouv_quantite = values.get('quantite', None)
		ancien_article_obj = self.env['production.article'].browse(self.article_id.id)

		if nouv_article:
			nouv_article_obj = self.env['production.article'].browse(nouv_article)
			if nouv_quantite:
				#test si quantite <= 0 on genere exception
				if nouv_quantite <= 0:
					raise Warning(_('Erreur!'), 
								_('La quantité doit étre supérieur strictement à zero'))

				#modifier stock
				ancien_article_obj.stock_reel -= self.quantite
				nouv_article_obj.stock_reel += nouv_quantite

			else:#si quantite non changer
				#modifier stock
				ancien_article_obj.stock_reel -= self.quantite
				nouv_article_obj.stock_reel += self.quantite
		else:#si article non changer
			if nouv_quantite:
				#test si quantite <= 0 on genere exception
				if nouv_quantite <= 0:
					raise Warning(_('Erreur!'), 
								_('La quantité doit étre supérieur strictement à zero'))

				#modifier stock
				ancien_article_obj.stock_reel += nouv_quantite - self.quantite


		obj_id=super(bon_entree_achat, self).write(values)

		return obj_id

	@api.multi
	def unlink(self):
		for rec in self:
			article_obj = self.env['production.article'].browse(rec.article_id.id)
			article_obj.stock_reel -= rec.quantite

		return super(bon_entree_achat, self).unlink()

	@api.onchange('commande_id')
	def onchange_commande_id(self):
		res = {}
		ids = []
		default_commande = self._context.get('default_commande_id', False)
		default_article = self._context.get('default_article_id', False)
		if self.commande_id:
			if default_article == False:
			    self.article_id = []
			if default_commande:
				if self.commande_id.id != default_commande:
					self.article_id = []
			#filter sur le champ article_id selon commande_id séléctionné
			for ligne in self.commande_id.article_commande_fournisseur_ids:
				ids.append(ligne.article_id.id)

		else:#si commande_id vide
			self.article_id = []

		res['domain'] = {'article_id': [('id', 'in', ids)]}
		return res
