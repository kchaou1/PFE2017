# -*- coding: utf-8 -*-

import sys
import openerp
from openerp import models, fields, api, _
from openerp import tools
from datetime import date
from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning



class article_commande_fournisseur_rel(models.Model):

	_name = "article.commande.fournisseur.rel"
	
	commande_fournisseur_id = fields.Many2one('commande.fournisseur', 'Commande', ondelete='cascade', required=True)
	article_id = fields.Many2one('production.article', 'Article', ondelete='cascade', required=True)
	quantite = fields.Float('Quantité commandé', required=True)
	unite = fields.Selection([('u','U'),
							  ('kg','Kg'),
							  ('m2','m²'),
							  ('m','m')], related='article_id.unite', readonly=True, string='Unite')

	quantite_livre = fields.Float(compute='_get_quantite_livre', string='Quantité livré', readonly=True)
	progress = fields.Float(compute='_get_progress', string='Progression')

	stock_reel = fields.Float(string='Stk_Réel', related='article_id.stock_reel')
	stock_disponible = fields.Float('Stk_Dispo', related='article_id.stock_disponible')
	stock_non_reserve = fields.Float(string='Stk_Non_Rés', related='article_id.stock_non_reserve')

	state = fields.Selection([('brouillon','Brouillon'),
				   			  ('annulee','Annulée'),
				   			  ('attente','En attente de réception'),
				   			  ('recu_partiel','Reçu partiellemet'),
				  			  ('recu_total','Reçu totalemet'),], 'Etat', related='commande_fournisseur_id.state')

	@api.one
	@api.depends('commande_fournisseur_id', 'article_id')
	def _get_quantite_livre(self):
		for line in self:
			qte = 0
			bea_ids = self.env['bon.entree.achat'].search([('commande_id', '=', line.commande_fournisseur_id.id), 
														   ('article_id', '=', line.article_id.id)])
			for bea in bea_ids:
				qte += bea.quantite
			self.quantite_livre = qte

	@api.one
	@api.depends('quantite', 'quantite_livre')
	def _get_progress(self):
		if self.quantite > 0 and self.quantite_livre > 0:
			self.progress = self.quantite_livre / self.quantite * 100
		else:
			self.progress = 0


	@api.model
	def create(self, values):
		#test si quantite <= 0 on genere exception
		if values['quantite'] <= 0:
			raise Warning(_('Erreur!'), 
						_("La quantité commandé doit étre supérieur strictement à zero"))

		new_id = super(article_commande_fournisseur_rel, self).create(values)
		return new_id

	@api.multi
	def write(self, values):
		obj_id=super(article_commande_fournisseur_rel, self).write(values)
		for obj in self:
			#test si quantite <= 0 on genere exception
			if obj.quantite <= 0:
				raise Warning(_('Erreur!'), 
							_("La quantité commandé doit étre supérieur strictement à zero"))

		return obj_id

	@api.multi
	def ajouter_bon_entree_achat(self):
		return {
			'name': _("Bon entrée achat"),
			'type': 'ir.actions.act_window',
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'bon.entree.achat',
			'view_id': False,
			'context': {
					'default_commande_id': self.commande_fournisseur_id.id,
					'default_article_id': self.article_id.id},
					}
