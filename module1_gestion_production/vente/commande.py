# -*- coding: utf-8 -*-

import sys
import openerp
from openerp import models, fields, api, _
from openerp import tools
from datetime import date
from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning




class production_commande(models.Model):

    @api.one
    @api.depends('state')
    def _check_color(self):
        for rec in self:
            color = 0
            color_value = self.env['color.status'].search([('state', '=', rec.state)], limit=1).color
            if color_value:
                color = color_value

            self.member_color = color

    #button workflow Démarrer
    @api.one
    def action_demarrer_commande(self):
        if self.article_commande_ids:
            self.write({'state': 'demarre'})
        else:
            raise Warning(_('Erreur!'), 
                        _('Cette commande (%s) ne contient aucun article')% (self.num_commande))
    @api.one
    def action_confirmer_commande(self):
        self.write({'state': 'nonplanifie'})

    #button workflow Terminer
    @api.one
    def action_terminer_commande(self):
        self.write({'state': 'termine'})

    _name = 'production.commande'
    _rec_name = 'num_commande'

    member_color = fields.Integer(compute='_check_color', string='Color')
    of_ids = fields.One2many('production.ordre.fabrication', 'commande_id', 'Ordres de fabrication')
    num_commande = fields.Char('Num commande', required=True)
    client_id = fields.Many2one('vente.client', 'Client', required=True, ondelete='cascade')
    date_creation = fields.Date('Date création', required=True, default= lambda *a:datetime.now().strftime('%Y-%m-%d'))
    date_limit_cmd = fields.Date('Date limite', required=True)
    article_commande_ids = fields.One2many('article.commande.rel', 'commande_id', 'Articles')
    state = fields.Selection([('nonconfirme','Non Confirmé'),('nonplanifie','Non Planifié'),('planifie','Planifié'),
                              ('demarre','Demarré'),
                              ('termine','Terminé')], 'Etat', readonly=True, default='nonconfirme')
    bon_livraison_ids = fields.One2many('bon.livraison', 'commande_id', 'Bons de livraiosn')
    bon_reservation_ids = fields.One2many('bon.reservation', 'commande_id', 'Bons de réservation')

    @api.model
    def create(self, values):
        #test num_commande doit etre unique
        if self.env['production.commande'].search_count([('num_commande', '=', values['num_commande'])]) > 0:
            raise Warning(_('Erreur!'),
                        _('Numéro commande existe déjà [ %s ].')% (values['num_commande']))

        # test date_creation <= date_limit_cmd
        if values['date_creation'] > values['date_limit_cmd']:
            raise Warning(_('Erreur!'),
                        _('Il faut que : Date création <= Date limite'))

        obj_id = super(production_commande, self).create(values)
        #test si les lignes articles sont distinct
        ids = []
        for obj in self.browse(obj_id.id):
            for line in obj.article_commande_ids:
                if line.article_id.id in ids:
                    raise Warning(_('Erreur!'), 
                                _("Même article ajouté plusieurs fois : %s") % line.article_id.code_article)
                ids.append(line.article_id.id)

        #récupérer les lignes de commande
        article_lines = self.env['article.commande.rel'].search([('commande_id', '=', obj_id.id)])
        for l in article_lines:
            #test date_creation <= date_limit (article) <= date_limit_cmd
            if l.date_limit > values['date_limit_cmd'] or l.date_limit < values['date_creation']:
                raise Warning(_('Erreur!'), 
                            _('Les dates des lignes articles doivent êtres dans [ %s , %s].\n %s qui est séléctionnée')% (values['date_creation'], values['date_limit_cmd'], l.date_limit))
            #vérifier quantité
            if float(l.quantite) <= 0:
                raise Warning(_('Erreur!'), 
                            _('La quantité doit être supérieur à zero'))

        return obj_id
    
    @api.multi
    def creer_of(self):

        return { 
            'name': _("Ordre fabrication"),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'production.ordre.fabrication',
            'view_id': False,
            'context': {
                    'default_commande_id': self.id,
                    },
            }


    @api.multi
    def write(self, values):
        obj_id=super(production_commande,self).write(values)
        for obj in self:
            #test num_commande doit etre unique
            self.env.cr.execute('select * from production_commande where num_commande = %s',(obj.num_commande,))
            lines = self.env.cr.dictfetchall()

            if len(lines) > 1:
                raise Warning(_('Erreur!'), 
                            _('Numéro commande existe déjà [ %s ].')% (obj.num_commande))
            # test date_creation <= date_limit_cmd
            if obj.date_creation > obj.date_limit_cmd:
                raise Warning(_('Erreur!'), 
                            _('Il faut que : Date création <= Date limite'))

            #test si les lignes articles sont distinct
            ids = []
            for line in obj.article_commande_ids:
                if line.article_id.id in ids:
                    raise Warning(_('Erreur!'), 
                                _("Même article ajouté plusieurs fois : %s") % line.article_id.code_article)
                ids.append(line.article_id.id)

            #récupérer les lignes de commande
            article_lines = self.env['article.commande.rel'].search([('commande_id', '=', obj.id)])
            for l in article_lines:
                #test date_creation <= date_limit (article) <= date_limit_cmd
                if l.date_limit > obj.date_limit_cmd or l.date_limit < obj.date_creation:
                    raise Warning(_('Erreur!'), 
                                _('Les dates des lignes articles doivent êtres dans [ %s , %s].\n %s qui est séléctionnée')% (obj.date_creation, obj.date_limit_cmd, l.date_limit))
                #vérifier commande
                if float(l.quantite) <= 0:
                    raise Warning(_('Erreur!'), 
                                _('La quantité doit être supérieur à zero'))

        return obj_id