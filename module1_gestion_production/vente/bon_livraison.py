# -*- coding: utf-8 -*-

import sys
import openerp
from openerp import models, fields, api, _
from openerp import tools
from datetime import date
from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning


class bon_livraison(models.Model):

    @api.one
    @api.depends('commande_id', 'article_id')
    def _get_quantite_commande(self):
        qte = 0
        if self.commande_id and self.article_id:
            self.quantite_commande = self.env['article.commande.rel'].search([('article_id', '=', self.article_id.id), 
                                                                              ('commande_id', '=', self.commande_id.id)], 
                                                                              limit=1).quantite

    @api.one
    @api.depends('commande_id', 'article_id')
    def _get_quantite_livre(self):
        qte = 0
        if self.commande_id and self.article_id:
            bon_livraison_ids = self.env['bon.livraison'].search([('commande_id', '=', self.commande_id.id), 
                                                                  ('article_id', '=', self.article_id.id)])
            qte = 0
            for bl in bon_livraison_ids:
                qte += bl.quantite

        self.quantite_livre = qte

    @api.one
    @api.depends('commande_id', 'article_id')
    def _get_quantite_reserve(self):
        qte = 0
        if self.commande_id and self.article_id:
            bon_reservation_ids = self.env['bon.reservation'].search([('commande_id', '=', self.commande_id.id), 
                                                                      ('article_id', '=', self.article_id.id)])
            qte = 0
            for br in bon_reservation_ids:
                qte += br.quantite

        self.quantite_reserve = qte

    @api.one
    @api.depends('quantite_commande', 'quantite_livre')
    def _get_progress_livre_commande(self):
        if self.quantite_commande > 0 and self.quantite_livre > 0:
            self.progress_livre_commande = self.quantite_livre / self.quantite_commande * 100
        else:
            self.progress_livre_commande = 0

    @api.one
    @api.depends('quantite_commande', 'quantite_reserve')
    def _get_progress_reserve_commande(self):
        if self.quantite_commande > 0 and self.quantite_reserve > 0:
            self.progress_reserve_commande = self.quantite_reserve / self.quantite_commande * 100
        else:
            self.progress_reserve_commande = 0

    _name = 'bon.livraison'

    code_bon = fields.Char('Code bon :', readonly=True)
    date_bon = fields.Date('Date bon', required=True, default= lambda *a:datetime.now().strftime('%Y-%m-%d'))
    client_id = fields.Many2one('vente.client', 'Code client', ondelete='cascade', required=True, domain=[('id', 'in', [])])
    commande_id = fields.Many2one('production.commande', 'Code commande', ondelete='cascade', required=True, 
                                    domain="[('state', '=', 'demarre')]" )
    article_id = fields.Many2one('production.article', 'Code article', ondelete='cascade', required=True)
    quantite = fields.Float('Quantité', required=True)

    quantite_commande = fields.Float(compute='_get_quantite_commande', string='Quantité commandé')
    quantite_commande2 = fields.Float('Quantité commandé', related='quantite_commande')
    quantite_livre = fields.Float(compute='_get_quantite_livre', string='Quantité livré')
    quantite_reserve = fields.Float(compute='_get_quantite_reserve', string='Quantité réservé')
    stock_disponible = fields.Float('Stock disponible', related='article_id.stock_disponible')
    stock_non_reserve = fields.Float('Stock non réservé', related='article_id.stock_non_reserve')

    unite = fields.Selection([('u','U'),
                              ('kg','Kg'),
                              ('m2','m²'),
                              ('m','m')], related='article_id.unite', readonly=True, string='Unite')
    progress_reserve_commande = fields.Float(compute='_get_progress_reserve_commande', string='Progression quantité réservé')
    progress_livre_commande = fields.Float(compute='_get_progress_livre_commande', string='Progression quantité livré')

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        res = super(bon_livraison, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        for field in res['fields']:
            if field == 'article_id':
                res['fields'][field]['domain'] = [('id','in', [])]

        return res

    @api.model
    def create(self, values):
        #test si quantite <= 0 on genere exception
        if values['quantite'] <= 0:
            raise Warning(_('Erreur!'), 
                        _('La quantité doit étre supérieur strictement à zero ( %s )')% (values['quantite']))

        #Calcul quantite réservé
        bon_reservation_ids = self.env['bon.reservation'].search([('commande_id', '=', values['commande_id']), 
                                                                ('article_id', '=', values['article_id'])])
        qte_res = 0
        for b in bon_reservation_ids:
            qte_res += b.quantite

        #test si aucun quantite réservé
        if qte_res == 0:
            raise Warning(_('Erreur!'), 
                        _('Aucun quantité réservé dans le stock'))

        #Calcul quantite livré
        bon_livraison_ids = self.env['bon.livraison'].search([('commande_id', '=', values['commande_id']), 
                                                              ('article_id', '=', values['article_id'])])
        qte_livre = 0
        for b in bon_livraison_ids:
            qte_livre += b.quantite

        #test si quantite livre > quantite reserve ==> exception
        qte_livre_total = qte_livre + values['quantite']
        if qte_livre_total > qte_res:
            raise Warning(_('Erreur!'), 
                        _('La quantité à livrer est supérieur à la quantité réservé:\n \
                        (quantite_à_livré : %s / quantite_réservé : %s)')% (qte_livre_total, qte_res))

        #generer code sequence "code_bon"
        values['code_bon'] = self.env['ir.sequence'].get('bon.livraison')

        # stock_reel -= qte
        # stock_reserve -= qte
        article_obj = self.env['production.article'].browse(values['article_id'])
        if article_obj:
            article_obj.stock_reel -= values['quantite']
            article_obj.stock_reserve -= values['quantite']

            #test stock minimale
            article_obj.verifier_stock()

        new_id = super(bon_livraison, self).create(values)

        return new_id

    @api.multi
    def write(self, values):

        commande = values.get('commande_id', None)
        if commande == None:
            commande = self.commande_id.id
        nouv_article = values.get('article_id', None)
        nouv_quantite = values.get('quantite', None)
        ancien_article_obj = self.env['production.article'].browse(self.article_id.id)

        if nouv_article:
            #Calcul quantite réservé
            bon_reservation_ids = self.env['bon.reservation'].search([('commande_id', '=', commande), 
                                                                    ('article_id', '=', nouv_article)])
            qte_res = 0
            for b in bon_reservation_ids:
                qte_res += b.quantite

            #Calcul quantite livré
            bon_livraison_ids = self.env['bon.livraison'].search([('commande_id', '=', commande), 
                                                                  ('article_id', '=', nouv_article)])
            qte_livre = 0
            for b in bon_livraison_ids:
                qte_livre += b.quantite

            nouv_article_obj = self.env['production.article'].browse(nouv_article)
            #si il y a une nouvelle quantité
            if nouv_quantite:
                #test si quantite <= 0 on genere exception
                if nouv_quantite <= 0:
                    raise Warning(_('Erreur!'), 
                                _('La quantité doit étre supérieur strictement à zero ( %s )')% (nouv_quantite))

                #test si quantite livre > quantite reserve ==> exception
                qte_livre_total = qte_livre + nouv_quantite
                if qte_livre_total > qte_res:
                    raise Warning(_('Erreur!'), 
                                _('La quantité à livrer est supérieur à la quantité réservé:\n \
                                (quantite_à_livré : %s / quantite_réservé : %s)')% (qte_livre_total, qte_res))


                #modifier le stock
                ancien_article_obj.stock_reel += self.quantite
                ancien_article_obj.stock_reserve += self.quantite
                nouv_article_obj.stock_reel -= nouv_quantite
                nouv_article_obj.stock_reserve -= nouv_quantite

            else:#meme quantite
                #test si quantite livre > quantite reserve ==> exception
                qte_livre_total = qte_livre + self.quantite
                if qte_livre_total > qte_res:
                    raise Warning(_('Erreur!'), 
                                _('La quantité à livrer est supérieur à la quantité réservé:\n \
                                (quantite_à_livré : %s / quantite_réservé : %s)')% (qte_livre_total, qte_res))

                #modifier le stock
                ancien_article_obj.stock_reel += self.quantite
                ancien_article_obj.stock_reserve += self.quantite
                nouv_article_obj.stock_reel -= self.quantite
                nouv_article_obj.stock_reserve -= self.quantite
        else:
            if nouv_quantite:
                #test si quantite <= 0 on genere exception
                if nouv_quantite <= 0:
                    raise Warning(_('Erreur!'), 
                                _('La quantité doit étre supérieur strictement à zero ( %s )')% (nouv_quantite))

                #Calcul quantite réservé
                bon_reservation_ids = self.env['bon.reservation'].search([('commande_id', '=', commande), 
                                                                        ('article_id', '=', self.article_id.id)])
                qte_res = 0
                for b in bon_reservation_ids:
                    qte_res += b.quantite

                #Calcul quantite livré
                bon_livraison_ids = self.env['bon.livraison'].search([('commande_id', '=', commande), 
                                                                      ('article_id', '=', self.article_id.id)])
                qte_livre = 0
                for b in bon_livraison_ids:
                    qte_livre += b.quantite

                #test si quantite livre > quantite reserve ==> exception
                if nouv_quantite > self.quantite:
                    qte_livre_total = qte_livre + nouv_quantite - self.quantite
                    if qte_livre_total > qte_res:
                        raise Warning(_('Erreur!'), 
                                    _('La quantité à livrer est supérieur à la quantité réservé:\n \
                                    (quantite_à_livré : %s / quantite_réservé : %s)')% (qte_livre_total, qte_res))

                #modifier le stock
                ancien_article_obj.stock_reel += self.quantite - nouv_quantite
                ancien_article_obj.stock_reserve += self.quantite - nouv_quantite


        obj_id=super(bon_livraison,self).write(values)

        return obj_id


    @api.multi
    def unlink(self):
        for rec in self:
            article_obj = self.env['production.article'].browse(rec.article_id.id)
            article_obj.stock_reel += rec.quantite
            article_obj.stock_reserve += rec.quantite

        return super(bon_livraison, self).unlink()
    
    

    @api.onchange('commande_id')
    def onchange_commande_id(self):
        res = {}
        ids = []
        default_commande = self._context.get('default_commande_id', False)
        default_article = self._context.get('default_article_id', False)
        if self.commande_id:
            if default_article == False:
                self.article_id = []
            if default_commande:
                if self.commande_id.id != default_commande:
                    self.article_id = []
            #filter sur le champ article_id selon commande_id séléctionné
            for ligne in self.commande_id.article_commande_ids:
                ids.append(ligne.article_id.id)

            #select client_id selon commande_id séléctionné
            self.client_id = self.commande_id.client_id

        else:#si commande_id vide
            self.article_id = []

        res['domain'] = {'article_id': [('id', 'in', ids)]}
        return res
