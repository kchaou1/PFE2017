# -*- coding: utf-8 -*-

import sys
import openerp
from openerp import models, fields, api, _
from openerp import tools
from datetime import date
from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning

#----------------------------------------------------------
# taux_tva
#----------------------------------------------------------
class taux_tva(models.Model):

    _name = 'taux.tva'
    _rec_name = 'taux_tva'

    client_ids = fields.One2many('vente.client', 'taux_tva_id', 'Clients')
    taux_tva = fields.Float('Taux TVA', required=True)
    default = fields.Boolean('Défaut')
 
    @api.model
    def create(self, values):
        if values['default'] == True:
            obj_ids = self.search([('default', '=', True)])
            if len(obj_ids) > 0:
                raise Warning(_('Erreur!'), 
                            _('Il faut un seul valeur par défaut'))
        #taux_tva doit etre unique
        taux_tva_count = self.search_count([('taux_tva', '=', values['taux_tva'])])
        if taux_tva_count > 0:
            raise Warning(_('Erreur!'), 
                        _('( %s ) : Cette valeur existe déja')% (values['taux_tva']))

        obj_id = super(taux_tva, self).create(values)
        return obj_id

    @api.multi
    def write(self, values):
        if values.get("default", False) == True:
            obj_ids = self.search([('default', '=', True)])
            if len(obj_ids) > 0:
                raise Warning(_('Erreur!'), 
                            _('Il faut un seul valeur par défaut'))
        #taux_tva doit etre unique
        if values.get("taux_tva", False) != False and values.get("taux_tva", False) != self.taux_tva:
            taux_tva_count  = self.search_count([('taux_tva', '=', values.get("taux_tva", False))])
            if taux_tva_count  > 0:
                raise Warning(_('Erreur!'), 
                            _('( %s ) : Cette valeur existe déja')% (values.get("taux_tva", False)))


        obj_id = super(taux_tva, self).write(values)
        return obj_id
