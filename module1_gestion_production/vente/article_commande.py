# -*- coding: utf-8 -*-

import sys
import openerp
from openerp import models, fields, api, _
from openerp import tools
from datetime import date
from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning





class article_commande_rel(models.Model):

    @api.one
    @api.depends('commande_id', 'article_id')
    def _get_quantite_livre(self):
        for rec in self:
            qte = 0
            bl_ids = self.env['bon.livraison'].search([('commande_id', '=', rec.commande_id.id), 
                                                       ('article_id', '=', rec.article_id.id)])
            for bl in bl_ids:
                qte += bl.quantite
            self.quantite_livre = qte

    @api.one
    @api.depends('commande_id', 'article_id')
    def _get_quantite_reserve(self):
        for rec in self:
            qte = 0
            br_ids = self.env['bon.reservation'].search([('commande_id', '=', rec.commande_id.id), 
                                                         ('article_id', '=', rec.article_id.id)])
            for br in br_ids:
                qte += br.quantite
            self.quantite_reserve = qte

    @api.one
    @api.depends('quantite', 'quantite_livre')
    def _get_progress(self):
        if self.quantite > 0 and self.quantite_livre > 0:
            self.progress = self.quantite_livre / self.quantite * 100
        else:
            self.progress = 0

    _name = "article.commande.rel"

    article_id = fields.Many2one('production.article', 'Article', ondelete='cascade', required=True)
    commande_id = fields.Many2one('production.commande', 'Commande', ondelete='cascade', required=True)
    quantite = fields.Float('Quantité', required=True)
    unite = fields.Selection([('u','U'),
                              ('kg','Kg'),
                              ('m2','m²'),
                              ('m','m')], related='article_id.unite', readonly=True, string='Unite')
    date_limit = fields.Date('Date limite', required=True)

    quantite_livre = fields.Float(compute='_get_quantite_livre', string='Qte_Livré')
    quantite_reserve = fields.Float(compute='_get_quantite_reserve', string='Qte_Rés')
    progress = fields.Float(compute='_get_progress', string='Progression')
    stock_non_reserve = fields.Float(string='Stk_Non_Rés', related='article_id.stock_non_reserve')

    @api.multi
    def creer_of(self):
        #pour creer un of il faut que la commande en etat demarre
        if self.commande_id.state != 'nonplanifie':
            raise Warning(_('Erreur!'),
                        _('OF est dejà Planifié'))
        return { 
            'name': _("Ordre fabrication"),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'production.ordre.fabrication',
            'view_id': False,
            'context': {
                    'default_commande_id': self.commande_id.id, 
                    'default_article_sortant': self.article_id.id,
                    'default_quantite': self.quantite,
                    'default_date_fin': self.date_limit,
                    'default_line_commande_id': self.id,
                    'default_famille_id':self.article_id.famille_id.id,
                    'default_quantite':self.quantite
                    },
            }

    @api.multi
    def creer_bon_reservation(self):
        #pour creer un bon réservation il faut que la commande en etat demarre
        if self.commande_id.state == 'planifie':
            raise Warning(_('Erreur!'),
                        _('La commande  n\'est pas encore démarré'))
        return { 
            'name': _("Bon de réservation"),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'bon.reservation',
            'view_id': False,
            'context': {
                    'default_client_id': self.commande_id.client_id.id, 
                    'default_commande_id': self.commande_id.id,
                    'default_article_id': self.article_id.id,
                    'default_quantite_commande': self.quantite
                    },
            }

    @api.multi
    def creer_bon_livraison(self):
        #pour creer un bon livraison il faut que la commande en etat demarre
        if self.commande_id.state == 'planifie':
            raise Warning(_('Erreur!'),
                        _('La commande  n\'est pas encore démarré'))
        return { 
            'name': _("Bon de livraison"),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'bon.livraison',
            'view_id': False,
            'context': {
                    'default_client_id': self.commande_id.client_id.id, 
                    'default_commande_id': self.commande_id.id,
                    'default_article_id': self.article_id.id,
                    'default_quantite_commande': self.quantite
                    },
            }