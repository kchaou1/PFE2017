# -*- coding: utf-8 -*-

import sys
import openerp
from openerp import models, fields, api, _
from openerp import tools
from datetime import date
from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.modules.module import get_module_resource
import xmlrpclib
import time
from time import mktime
import timestring
import ast






class production_ordre_fabrication(models.Model):

    @api.multi
    @api.depends('article_sortant', 'code_of', 'code_operateur', 'code_machine', 'quantite')
    def name_get(self):
        result = []
        for record in self:
            result.append((record.id, record.code_of + u' { opérateur:' + record.code_operateur.code_operateur + ', machine:' + record.code_machine.code_machine + ', production: ' + str(record.quantite) + ' de ' + record.article_sortant.code_article + ' }')) 
        return result


    #button workflow Démarrer
    @api.one
    def action_demarrer_fabrication(self):
        print '**************************'
        print self.commande_id.id
        wt = self.env['production.commande']
        wt2 = wt.browse(self.commande_id.id)
        wt2.write({'state': 'demarre'})
        self.write({'state': 'demarre'})
        for of in self:
            if of.demarre == False:
                print of.demarre
                of.demarre = True
                print of.demarre
                of.article_entree1.stock_reserve += of.quantite1
                if of.article_sortant.famille_id.article_entree2 == True:
                    of.article_entree2.stock_reserve += of.quantite2


#       for of in self:
#           #on ne peut demarrer un of que si les quantites entrantes soient réservé
#           if of.quantite1 > of.quantite_reserve_entree1:
#               raise Warning(_('Erreur!'), 
#                           _('La quantité entrée1 n\'est pas encore réservé'))
#           if of.article_sortant.famille_id.article_entree2 == True:
#               if of.quantite2 > of.quantite_reserve_entree2:
#                   raise Warning(_('Erreur!'), 
#                               _('La quantité entrée2 n\'est pas encore réservé'))
    

    #button workflow Arréter
    @api.one
    def action_arreter_fabrication(self):
        self.write({'state': 'planifie'})

    #button workflow Terminer
    @api.one
    def action_terminer_fabrication(self):
        self.write({'state': 'termine'})
        #wt = self.env['production.commande']
        #wt2 = wt.browse(self.commande_id.id)
        #wt2.write({'state': 'termine'})

#   @api.onchange('activite_id')
#   def onchange_activite(self):
#       res = {}
#       ids = []
#       if self.env.context.get('default_article_sortant', False) == False:
#           #filter sur les articles sortants selon l'activité selectionnée
#           self.article_sortant = []
#           if self.activite_id:
#               for famille in self.activite_id.activite_famille_sortant_ids:
#                   for article in famille.article_ids:
#                       ids.append(article.id)
#           else:
#               self.env.cr.execute('SELECT id FROM production_article')
#               article_objs = self.env.cr.fetchall()
#               for op in article_objs:
#                   ids.append(op)
#           res['domain'] = {'article_sortant': [('id', 'in', ids)]}
#       return res
    @api.multi
    @api.onchange('activite_id')
    def onchange_activite(self):
        famille_sortant_ids=[]
        res={}
        activite_ids=self.env['production.activite'].search([('id','=',self.activite_id.id)])
        for activite in activite_ids:
            famille_sortant_ids=activite.activite_famille_sortant_ids.ids
        res['domain']={'famille_id':[('id','in',famille_sortant_ids)]}
        return res    
           

    @api.onchange('famille_id')
    def onchange_famille(self):
        res = {}
        ids = []
        if self.env.context.get('default_article_sortant', False) == False:
            #filter sur les articles sortants selon la famille selectionnée
            self.article_sortant = []
            if self.famille_id:
                for article in self.famille_id.article_ids:
                    ids.append(article.id)
            else:
                self.env.cr.execute('SELECT id FROM production_article')
                article_objs = self.env.cr.fetchall()
                for op in article_objs:
                    ids.append(op)
            res['domain'] = {'article_sortant': [('id', 'in', ids)]}
        return res

    @api.one
    def _get_quantite_realiser(self):
        for of in self:
            bef_ids = self.env['production.bon.entree.fabrication'].search([('code_of', '=', of.id)])
            qte = 0
            for bef in bef_ids:
                qte = qte + bef.quantite
            self.quantite_realiser = qte

    @api.one
    @api.depends('quantite', 'quantite_realiser')
    def _get_progress(self):
        if self.quantite > 0 and self.quantite_realiser > 0:
            self.progress = self.quantite_realiser / self.quantite * 100
        else:
            self.progress = 0

    #method pour filter "OF Prêt à fabriquer"
    @api.one
    @api.depends('quantite1', 'quantite_stock_article_entree1', 'quantite2', 'quantite_stock_article_entree2')
    def _article_entree_exist(self):
        for of in self:
            if of.article_sortant.famille_id.article_entree2 == True:
                if of.quantite1 <= of.quantite_stock_article_entree1 and of.quantite2 <= of.quantite_stock_article_entree2:
                    self.article_entree_exist = True
                else:
                    self.article_entree_exist = False
            else:
                if of.quantite1 <= of.quantite_stock_article_entree1:
                    self.article_entree_exist = True
                else:
                    self.article_entree_exist = False

    @api.one
    @api.depends('state')
    def _check_color(self):
        for rec in self:
            color = 0
            color_value = self.env['color.status'].search([('state', '=', rec.state)], limit=1).color
            if color_value:
                color = color_value

            self.member_color = color

    @api.one
    def _get_bon_entree_count(self):
        for of in self:
            self.bon_entree_count  = self.env['production.bon.entree.fabrication'].search_count([('code_of', '=', of.id)])

    @api.one
    @api.depends('article_entree1')
    def _get_quantite_reserve_entree1(self):
        qte = 0
        if self.article_entree1:
            bon_reservation_ids = self.env['bon.reservation.ordre.fabrication'].search([
                                                                                ('ordre_fabrication_id','=',self.id), 
                                                                                ('article_id', '=', self.article_entree1.id)])
            for br in bon_reservation_ids:
                qte += br.quantite

        self.quantite_reserve_entree1 = qte

    @api.one
    @api.depends('article_entree2')
    def _get_quantite_reserve_entree2(self):
        qte = 0
        if self.article_entree2:
            bon_reservation_ids = self.env['bon.reservation.ordre.fabrication'].search([('ordre_fabrication_id','=',self.id), 
                                                                                ('article_id', '=', self.article_entree2.id)])
            for br in bon_reservation_ids:
                qte += br.quantite

        self.quantite_reserve_entree2 = qte
    @api.one
    def _get_conversion(self):
        self.conversion=""
    
    @api.model
    def _get_date(self):
        return datetime.now().strftime('%Y-%m-%d')

    def _get_date_fin(self):
        cadence=0
        quantite=0
        username = 'admin' #the user
        pwd = 'admin'      #the password of the user
        dbname = 'odoo'

        print '######### quantite ############'
        print self.quantite
        sock_common = xmlrpclib.ServerProxy ('http://localhost:8069/xmlrpc/common')
        uid = sock_common.login(dbname, username, pwd)
        
        sock = xmlrpclib.ServerProxy('http://localhost:8069/xmlrpc/object')

        args1 = [] #query clause
        ids1 = sock.execute(dbname, uid, pwd, 'reglage.date', 'search', args1)  
        data1 = sock.execute(dbname, uid, pwd, 'reglage.date', 'read', ids1)
        args2 = []
        ids2 = sock.execute(dbname, uid, pwd, 'article.machine.rel', 'search', args2)

        print '############################## code machine################'
        print self.code_machine.code_machine
        print self.article_sortant.code_article
       
        data2 = sock.execute(dbname, uid, pwd, 'article.machine.rel', 'read', ids2)
        for x in data2:
            print '######################## cadence ################################'
           
            if x['machine_id']!=False:
               cod_machine_1=x['machine_id'][1]
               
               if cod_machine_1[cod_machine_1.index("(") + 1:cod_machine_1.rindex(")")]==self.code_machine.code_machine:
                  if self.article_sortant.code_article==x['article_id'][1]:
                     cadence=x['cadence'] 
                     print x['cadence']
            
            
        for y in data1:
            if y['jour']=='lundi':
               monday=int(y['heures'])
            if y['jour']=='mardi':
               tuesday=int(y['heures'])
               
            if y['jour']=='mercredi':
               wednesday=int(y['heures'])
            if y['jour']=='jeudi':
               thursday=int(y['heures'])

            if y['jour']=='vendredi':
                friday=int(y['heures'])
            if y['jour']=='samedi':
               saturday=int(y['heures'])         



       
        
        
        reglage=4
        print self.date_debut
        date_debut=time.strptime(self.date_debut, '%Y-%m-%d')
        i=datetime.fromtimestamp(mktime(date_debut))
        print i.strftime('%A')
        quantite=self.quantite
        duree=0
        if quantite==0 :
           raise Warning(_('Erreur!'), 
                            _('la quantite doit etre superieur a zero'))
          
        if cadence !=0:
           if int(quantite)>0 and int(cadence)>0 :
              duree=(int(quantite)/int(cadence))+reglage
           else:
              raise Warning(_('Erreur!'), 
                             _('duree ne doit pas etre egale a zero')) 
        

        print '#### duree #####'
        print duree
        duree_restante=duree
        while duree_restante>0:
             if i.strftime('%A')=='lundi':
                date_max = datetime(2014, 5, 12, 10, 30)
                
                duree_restante=duree-monday
                print "######## duree ##########"
                print duree
                print "####### duree_restante ######"
                print duree_restante
                #print added_time
                #print date_max.time()
                if duree_restante>0 and duree>0:
                   
                    i = i + timedelta(days=1)
                     #print i
                    duree=duree_restante
                else:
                   print i
                   self.date_fin=i 
                   break
             if i.strftime('%A')=='mardi':
                date_max = datetime(2014, 5, 12, 10, 30)
                
                duree_restante=duree-tuesday
                print "######## duree ##########"
                print duree
                print "####### duree_restante ######"
                print duree_restante
                #print added_time
                #print date_max.time()
                if duree_restante>0 and duree>0:
                   
                    i = i + timedelta(days=1)
                     #print i
                    duree=duree_restante
                else:
                   print i
                   self.date_fin=i
                   break 
             if i.strftime('%A')=='mercredi':
                date_max = datetime(2014, 5, 12, 16, 30)
                duree_restante=duree-wednesday
                print "######## duree ##########"
                print duree
                print "####### duree_restante ######"
                print duree_restante
                if duree_restante>0 and duree>0:
                   
                    i = i + timedelta(days=1)
                     #print i
                    duree=duree_restante
                else:
                   print i
                   self.date_fin=i 
                   break
                   
             if i.strftime('%A')=='jeudi':
                date_max = datetime(2014, 5, 12, 16, 30)
                duree_restante=duree-thursday
                print "######## duree ##########"
                print duree
                print "####### duree_restante ######"
                print duree_restante
                if duree_restante>0 and duree>0:
                   
                    i = i + timedelta(days=1)
                     #print i
                    duree=duree_restante
                else:
                   print i
                   self.date_fin=i
                   break 
             if i.strftime('%A')=='vendredi':
                date_max = datetime(2014, 5, 12, 16, 30)
                duree_restante=duree-friday
                print "######## duree ##########"
                print duree
                print "####### duree_restante ######"
                print duree_restante
                if duree_restante>0 and duree>0:
                   
                    i = i + timedelta(days=1)
                     #print i
                    duree=duree_restante
                else:
                   print i
                   self.date_fin=i
                   break 
             if i.strftime('%A')=='samedi':                 
                date_max = datetime(2014, 5, 12, 16, 30)
                duree_restante=duree-saturday
                print "######## duree ##########"
                print duree
                print "####### duree_restante ######"
                print duree_restante

                if duree_restante>0 and duree>0:
                   
                    i = i + timedelta(days=1)
                     #print i
                    duree=duree_restante
                else:
                   print i 
                   self.date_fin=i
                   break   
             if i.strftime('%A')=='dimanche':                 
                date_max = datetime(2014, 5, 12, 16, 30)
                duree_restante=duree-saturday
                print "######## duree ##########"
                print duree
                print "####### duree_restante ######"
                print duree_restante

                if duree_restante>0 and duree>0:
                   
                    i = i + timedelta(days=1)
                    duree=duree_restante
                else:
                   print i 
                   self.date_fin=i
                   break        
                   


    _name = 'production.ordre.fabrication'
    
    bon_entree_fabrication_ids = fields.One2many('production.bon.entree.fabrication', 'code_of', 'Bons entrées fabrication')
    bon_reservation_of_ids = fields.One2many('bon.reservation.ordre.fabrication', 'ordre_fabrication_id', 'Bons de réservation')
    bon_entree_count = fields.Integer(compute='_get_bon_entree_count', string='Bons entrées')
    member_color = fields.Integer(compute='_check_color', string='Color')
    activite_id = fields.Many2one('production.activite', 'Activité', ondelete='cascade')
    famille_id = fields.Many2one('production.famille', 'Famille', ondelete='cascade')
    code_of = fields.Char('Code OF :', readonly=True)
    commande_id = fields.Many2one('production.commande', 'Num commande', ondelete='cascade', domain=[('state', '!=', 'termine')])
    line_commande_id = fields.Many2one('article.commande.rel', 'Article commandé', ondelete='cascade')
    article_sortant = fields.Many2one('production.article', 'Article sortant', ondelete='cascade', required=True)
    quantite = fields.Float('Quantité', required=True)
###
    unite_quantite_sortant = fields.Selection([('u','U'),
                                              ('kg','Kg'),
                                              ('m2','m²'),
                                              ('m','m')],string='Unité par Default')
    code_machine = fields.Many2one('production.machine', 'Code Machine', ondelete='cascade', required=True)
    code_operateur = fields.Many2one('production.operateur', 'Code Opérateur', ondelete='cascade', required=True)

    article_entree1 = fields.Many2one('production.article', 'Article entrée1', ondelete='cascade', required=True)
    quantite1 = fields.Float('Quantité1', required=True)
    grade_entree1 = fields.Char('Grade entrée1', default='Selon Référentiel')

    article_entree2 = fields.Many2one('production.article', 'Article entrée2', ondelete='cascade')
    quantite2 = fields.Float('Quantité2')
    grade_entree2 = fields.Char('Grade entrée2', default='Selon Référentiel')

    #champ somme la quantite realise
    quantite_realiser = fields.Integer(compute='_get_quantite_realiser', string='Quantité réalisée', default=0)
    progress = fields.Float(compute='_get_progress', string='Progression', readonly=True, digits=(16,2))

    nbre_par_paquet = fields.Integer('Nombre par paquet')
    grade_sortie = fields.Char('Grade sortie', default='Selon Référentiel')
    tolerance_qte_plus = fields.Char('Tolérance qte plus')
    tolerance_qte_moins = fields.Char('Tolérance qte moins')
    tolerance_dimension = fields.Char('Tolérance dimension')
    cerclage = fields.Boolean('Cerclage')
    zone_stockage_id = fields.Many2one('production.zone.stockage', 'Zone stockage', ondelete='cascade')
    date_debut = fields.Date('Date début', required=True, default= lambda *a:datetime.now().strftime('%Y-%m-%d'))
    date_fin = fields.Date('Date fin', default=lambda self: self._get_date() ,required=True)
    cadence = fields.Float('Cadence')
    remarque = fields.Text('Remarque')
    state = fields.Selection([('planifie','Planifié'),
                              ('demarre','Demarré'),
                              ('termine','Terminé')], 'Etat', readonly=True, default='planifie')
    unite_article_sortant = fields.Selection([('u','U'),
                                              ('kg','Kg'),
                                              ('m2','m²'),
                                              ('m','m')], related='article_sortant.unite')
    quantite_stock_article_sortant = fields.Float('Stk_dispo', related='article_sortant.stock_disponible')
    unite_article_entree1 = fields.Selection([('u','U'),
                                              ('kg','Kg'),
                                              ('m2','m²'),
                                              ('m','m')], related='article_entree1.unite')
    quantite_stock_article_entree1 = fields.Float('En Stock', related='article_entree1.stock_disponible')

    unite_article_entree2 = fields.Selection([('u','U'),
                                              ('kg','Kg'),
                                              ('m2','m²'),
                                              ('m','m')], related='article_entree2.unite')
    quantite_stock_article_entree2 = fields.Float('En Stock', related='article_entree2.stock_disponible')

    article_entree_exist = fields.Boolean(compute='_article_entree_exist', string='Entree exist', store=True)

    article_famille_article_entree2 = fields.Boolean(string='Article entrée2 ?',                                                    related='article_sortant.famille_id.article_entree2')
    article_famille_tolerance_qte_plus = fields.Boolean(string='tolerance_qte_plus ?',
                                                related='article_sortant.famille_id.tolerance_qte_plus')
    article_famille_tolerance_qte_moins = fields.Boolean(string='tolerance_qte_moins ?',
                                                related='article_sortant.famille_id.tolerance_qte_moins')
    article_famille_tolerance_dimension = fields.Boolean(string='tolerance_dimension ?',
                                                related='article_sortant.famille_id.tolerance_dimension')

    quantite_reserve_entree1 = fields.Float(compute='_get_quantite_reserve_entree1', string='Réservé')
    quantite_reserve_entree2 = fields.Float(compute='_get_quantite_reserve_entree2', string='Réservé')
    #champ defini l'etat du of demarré ou pas
    demarre = fields.Boolean(default=False)
    
    #unite de convertion 
    unite_article_a_convertir = fields.Selection([('u','U'), ('kg','Kg'),('m2','m²'), ('m','m')],'Unité De Saisie ' )
    qte_unite_article_a_convertir = fields.Float('Quantité')
    conversion = fields.Char(compute='_get_conversion',string='      ')
     
    @api.onchange('quantite')
    def onchange_texte(self):
        localdict = {
                    'un':self.article_sortant.unite,
                    'qs' : self.quantite,
                    'pu' : self.article_sortant.poids_unit,
                    'mc' : self.article_sortant.dimension_m2,
                    'pl' : self.article_sortant.poids_lineaire,
                    'd' : self.article_sortant.diametre,
                    'dl' : self.article_sortant.diametre_longitudinal,
                    'mt' : self.article_sortant.maille_transversal,
                    'ml' : self.article_sortant.maille_longitudinal,
                    'la' : self.article_sortant.largeur,
                    'lo' : self.article_sortant.longueur,
                    'nl' : self.article_sortant.nbr_Barres_longitudinales,
                    'nt' : self.article_sortant.nbr_Barres_transversales,

                    'q1' : self.quantite1,
                    'pu1' : self.article_entree1.poids_unit,
                    'mc1' : self.article_entree1.dimension_m2,
                    'pl1' : self.article_entree1.poids_lineaire,
                    'd1' : self.article_entree1.diametre,
                    'dl1' : self.article_entree1.diametre_longitudinal,
                    'mt1' : self.article_entree1.maille_transversal,
                    'ml1' : self.article_entree1.maille_longitudinal,
                    'la1' : self.article_entree1.largeur,
                    'lo1' : self.article_entree1.longueur,
                    'nl1' : self.article_entree1.nbr_Barres_longitudinales,
                    'nt1' : self.article_entree1.nbr_Barres_transversales,

                    'q2' : self.quantite2,
                    'pu2' : self.article_entree2.poids_unit,
                    'mc2' : self.article_entree2.dimension_m2,
                    'pl2' : self.article_entree2.poids_lineaire,
                    'd2' : self.article_entree2.diametre,
                    'dl2' : self.article_entree2.diametre_longitudinal,
                    'mt2' : self.article_entree2.maille_transversal,
                    'ml2' : self.article_entree2.maille_longitudinal,
                    'la2' : self.article_entree2.largeur,
                    'lo2' : self.article_entree2.longueur,
                    'nl2' : self.article_entree2.nbr_Barres_longitudinales,
                    'nt2' : self.article_entree2.nbr_Barres_transversales,
                    }
        if self.unite_article_a_convertir!=localdict['un']:
             #faire conversion
             if self.unite_article_a_convertir!=False:
                qte=self.convert(self.unite_article_a_convertir,localdict['un'],localdict['qs'],localdict['pu'],localdict['mc'],localdict['pl'])
                self.qte_unite_article_a_convertir=qte
                strr= str(self.quantite)+'  '+self.unite_article_a_convertir+' ---->  '+ str(self.qte_unite_article_a_convertir)+'  '+localdict['un']
                #change la valeur de champs conversion
                self.conversion=strr
            
            
        else:
             self.conversion=''
             if self.unite_article_a_convertir!=False:
                self.qte_unite_article_a_convertir=self.quantite
            
            



    @api.onchange('unite_article_a_convertir')
    def onchange_convertion(self):
        
        localdict = {
                    'un':self.article_sortant.unite,
                    'qs' : self.quantite,
                    'pu' : self.article_sortant.poids_unit,
                    'mc' : self.article_sortant.dimension_m2,
                    'pl' : self.article_sortant.poids_lineaire,
                    'd' : self.article_sortant.diametre,
                    'dl' : self.article_sortant.diametre_longitudinal,
                    'mt' : self.article_sortant.maille_transversal,
                    'ml' : self.article_sortant.maille_longitudinal,
                    'la' : self.article_sortant.largeur,
                    'lo' : self.article_sortant.longueur,
                    'nl' : self.article_sortant.nbr_Barres_longitudinales,
                    'nt' : self.article_sortant.nbr_Barres_transversales,

                    'q1' : self.quantite1,
                    'pu1' : self.article_entree1.poids_unit,
                    'mc1' : self.article_entree1.dimension_m2,
                    'pl1' : self.article_entree1.poids_lineaire,
                    'd1' : self.article_entree1.diametre,
                    'dl1' : self.article_entree1.diametre_longitudinal,
                    'mt1' : self.article_entree1.maille_transversal,
                    'ml1' : self.article_entree1.maille_longitudinal,
                    'la1' : self.article_entree1.largeur,
                    'lo1' : self.article_entree1.longueur,
                    'nl1' : self.article_entree1.nbr_Barres_longitudinales,
                    'nt1' : self.article_entree1.nbr_Barres_transversales,

                    'q2' : self.quantite2,
                    'pu2' : self.article_entree2.poids_unit,
                    'mc2' : self.article_entree2.dimension_m2,
                    'pl2' : self.article_entree2.poids_lineaire,
                    'd2' : self.article_entree2.diametre,
                    'dl2' : self.article_entree2.diametre_longitudinal,
                    'mt2' : self.article_entree2.maille_transversal,
                    'ml2' : self.article_entree2.maille_longitudinal,
                    'la2' : self.article_entree2.largeur,
                    'lo2' : self.article_entree2.longueur,
                    'nl2' : self.article_entree2.nbr_Barres_longitudinales,
                    'nt2' : self.article_entree2.nbr_Barres_transversales,
                    }
        if self.unite_article_a_convertir!=localdict['un']:
             #faire conversion
             if self.unite_article_a_convertir!=False:
                qte=self.convert(self.unite_article_a_convertir,localdict['un'],localdict['qs'],localdict['pu'],localdict['mc'],localdict['pl'])
                self.qte_unite_article_a_convertir=qte
                strr= str(self.quantite)+'  '+self.unite_article_a_convertir+' ---->  '+ str(self.qte_unite_article_a_convertir)+'  '+localdict['un']
                #change la valeur de champs conversion
                self.conversion=strr
            
            
        else:
             self.conversion=''
             if self.unite_article_a_convertir!=False:
                self.qte_unite_article_a_convertir=self.quantite

        

    @api.model
    def create(self, values):
      

      

        #print '*****************************************'
        #print values['commande_id']
        #print '*****************************************'
        #self.env['production.commande'].browse(values['commande_id']).write({'state': 'planifie'})
        wt = self.env['production.commande']

        wt2 = wt.browse(values['commande_id'])

        wt2.write({'state': 'planifie'})
        if values['quantite'] <= 0:
            raise Warning(_('Erreur!'), 
                        _('La quantité sortant doit étre supérieur strictement à zero ( %s )')% (values['quantite']))

        #test si quantite article_entrant1 <= 0 on genere exception
        if values['quantite1'] <= 0:
            raise Warning(_('Erreur!'), 
                        _('La quantité entrant1 doit étre supérieur strictement à zero ( %s )')% (values['quantite1']))

        #test si quantite article_entrant2 <= 0 on genere exception
        if values['article_famille_article_entree2'] == True:
            if values['quantite2'] <= 0:
                raise Warning(_('Erreur!'), 
                            _('La quantité entrant2 doit étre supérieur strictement à zero ( %s )')% (values['quantite2']))

        #test date debut < date fin
        if values['date_fin'] < values['date_debut']:
            raise Warning(_('Erreur!'), 
                        _('Date début doit être inférieur ou égal à la Date fin'))
        #generer code sequence "code_of"
        values['code_of'] = self.env['ir.sequence'].get('production.ordre.fabrication')
        id_of=super(production_ordre_fabrication, self).create(values)
        self.env['bon.reservation.ordre.fabrication'].create({'code_bon':'OF','date_bon':datetime.now().strftime('%Y-%m-%d'),'ordre_fabrication_id':id_of.id,'article_id':values['article_entree1'],'quantite':values['quantite1'],'quantite_demande':values['quantite1']})
        
        if values['article_entree2']:
           self.env['bon.reservation.ordre.fabrication'].create({'code_bon':'OF','date_bon':datetime.now().strftime('%Y-%m-%d'),'ordre_fabrication_id':id_of.id,'article_id':values['article_entree2'],'quantite':values['quantite2'],'quantite_demande':values['quantite2']})
       
        
        return id_of

    @api.multi
    def write(self, values):
        obj_id=super(production_ordre_fabrication, self).write(values)
        for obj in self:
            #test si quantite article_sortant <= 0 on genere exception
            if obj.quantite <= 0:
                raise Warning(_('Erreur!'), 
                            _('La quantité sortant doit étre supérieur strictement à zero ( %s )')% (obj.quantite))
            #test si quantite article_entrant1 <= 0 on genere exception
            if obj.quantite1 <= 0:
                raise Warning(_('Erreur!'), 
                            _('La quantité entrant1 doit étre supérieur strictement à zero ( %s )')% (obj.quantite1))
            #test si quantite article_entrant2 <= 0 on genere exception
            if obj.article_sortant.famille_id.article_entree2 == True:
                if obj.quantite2 <= 0:
                    raise Warning(_('Erreur!'), 
                                _('La quantité entrant2 doit étre supérieur strictement à zero ( %s )')% (obj.quantite2))
            #test date debut < date fin
            if obj.date_fin < obj.date_debut:
                raise Warning(_('Erreur!'), 
                            _('Date début doit être inférieur ou égal à la Date fin'))

        return obj_id


    @api.onchange('article_sortant','quantite','article_entree1','article_entree2')
   
    def onchange_formule(self):
        self.quantite1 = 0
        self.quantite2 = 0
        localdict = {
                    'qs' : self.quantite,
                    'pu' : self.article_sortant.poids_unit,
                    'mc' : self.article_sortant.dimension_m2,
                    'pl' : self.article_sortant.poids_lineaire,
                    'd' : self.article_sortant.diametre,
                    'dl' : self.article_sortant.diametre_longitudinal,
                    'mt' : self.article_sortant.maille_transversal,
                    'ml' : self.article_sortant.maille_longitudinal,
                    'la' : self.article_sortant.largeur,
                    'lo' : self.article_sortant.longueur,
                    'nl' : self.article_sortant.nbr_Barres_longitudinales,
                    'nt' : self.article_sortant.nbr_Barres_transversales,

                    'q1' : self.quantite1,
                    'pu1' : self.article_entree1.poids_unit,
                    'mc1' : self.article_entree1.dimension_m2,
                    'pl1' : self.article_entree1.poids_lineaire,
                    'd1' : self.article_entree1.diametre,
                    'dl1' : self.article_entree1.diametre_longitudinal,
                    'mt1' : self.article_entree1.maille_transversal,
                    'ml1' : self.article_entree1.maille_longitudinal,
                    'la1' : self.article_entree1.largeur,
                    'lo1' : self.article_entree1.longueur,
                    'nl1' : self.article_entree1.nbr_Barres_longitudinales,
                    'nt1' : self.article_entree1.nbr_Barres_transversales,

                    'q2' : self.quantite2,
                    'pu2' : self.article_entree2.poids_unit,
                    'mc2' : self.article_entree2.dimension_m2,
                    'pl2' : self.article_entree2.poids_lineaire,
                    'd2' : self.article_entree2.diametre,
                    'dl2' : self.article_entree2.diametre_longitudinal,
                    'mt2' : self.article_entree2.maille_transversal,
                    'ml2' : self.article_entree2.maille_longitudinal,
                    'la2' : self.article_entree2.largeur,
                    'lo2' : self.article_entree2.longueur,
                    'nl2' : self.article_entree2.nbr_Barres_longitudinales,
                    'nt2' : self.article_entree2.nbr_Barres_transversales,
                    }
        if self.article_sortant.famille_id.article_entree2 == True:
            #Calcul q1 et q2 (2 articles entrant)
            if self.article_sortant and self.article_entree1 and self.article_entree2 and self.quantite > 0:
                for param in self.article_sortant.famille_id.famille_entrant_param2_ids:
                    if param.famille_entrant1_id.id == self.article_entree1.famille_id.id and param.famille_entrant2_id.id == self.article_entree2.famille_id.id:
                        # 1 # si unite <> unite_qs ==> convert
                        if self.article_sortant.unite <> param.unite_qs:
                            localdict['qs'] = self.convert(self.article_sortant.unite, param.unite_qs, self.quantite, localdict.get("pu", None), localdict.get("mc", None), localdict.get("pl", None))
                        # 2 # execution du formule q1
                        try:
                            localdict['q1'] = eval(param.calcul_quantite1, localdict)
                        except:
                            pass
                        # 3 # execution du formule q2
                        try:
                            localdict['q2'] = eval(param.calcul_quantite2, localdict)
                        except:
                            pass

                        # 4 # si unite <> unite_q1 ==> convert
                        if self.article_entree1.unite <> param.unite_q1:
                            localdict['q1'] = self.convert(param.unite_q1, self.article_entree1.unite, localdict.get("q1", None), localdict.get("pu1", None), localdict.get("mc1", None), localdict.get("pl1", None))
                        # 5 # si unite <> unite_q2 ==> convert
                        if self.article_entree2.unite <> param.unite_q2:
                            localdict['q2'] = self.convert(param.unite_q2, self.article_entree2.unite, localdict.get("q2", None), localdict.get("pu2", None), localdict.get("mc2", None), localdict.get("pl2", None))

                        break
        else:
            #Calcul q1 (un seul article entrant)
            if self.article_sortant and self.article_entree1 and self.quantite > 0:
                for param in self.article_sortant.famille_id.famille_entrant_param_ids:
                    if param.famille_entrant1_id.id == self.article_entree1.famille_id.id:
                        # 1 # si us1 <> us2 ==> convert
                        if self.article_sortant.unite <> param.unite_qs:
                            localdict['qs'] = self.convert(self.article_sortant.unite, param.unite_qs, self.quantite, localdict.get("pu", None), localdict.get("mc", None), localdict.get("pl", None))
                        # 2 # execution du formule q1
                        try:
                            localdict['q1'] = eval(param.calcul_quantite1, localdict)
                        except:
                            pass

                        # 3 # si ue1 <> ue2 ==> convert
                        if self.article_entree1.unite <> param.unite_q1:
                            localdict['q1'] = self.convert(param.unite_q1, self.article_entree1.unite, localdict.get("q1", None), localdict.get("pu1", None), localdict.get("mc1", None), localdict.get("pl1", None))

                        break
        self.quantite1 = localdict.get("q1", None)  
        self.quantite2 = localdict.get("q2", None)

    #method qui convert qte en (u1) vers (u2)
    def convert(self, u1, u2, qte, pu, mc, pl):
        if u1 == "m":
            if u2 == "kg":
                qte = qte * pl
            else:
                if u2 == "u":
                    qte = qte * pl / pu
                else:
                    if u2 == "m2":
                        qte = qte * pl / pu * mc
        else:
            if u1 == "kg":
                if u2 == "m":
                    qte = qte / pl
                else:
                    if u2 == "u":
                        qte = qte / pu
                    else:
                        if u2 == "m2":
                            qte = qte / pu * mc
            else:
                if u1 == "u":
                    if u2 == "m":
                        qte = qte * pu / pl
                    else:
                        if u2 == "kg":
                            qte = qte * pu
                        else:
                            if u2 == "m2":
                                qte = qte * mc
                else:
                    if u1 == "m2":
                        if u2 == "m":
                            qte = qte / mc * pu / pl
                        else:
                            if u2 == "kg":
                                qte = qte / mc * pu
                            else:
                                if u2 == "u":
                                    qte = qte / mc
        return qte

    def valider_condition(self, article_ids, conditions, article_sortant, e):
        res_article_ids = []
        localdict = {
                    'pu' : article_sortant.poids_unit,
                    'mc' : article_sortant.dimension_m2,
                    'pl' : article_sortant.poids_lineaire,
                    'd' : article_sortant.diametre,
                    'dl' : article_sortant.diametre_longitudinal,
                    'mt': article_sortant.maille_transversal,
                    'ml': article_sortant.maille_longitudinal,
                    'la' : article_sortant.largeur,
                    'lo' : article_sortant.longueur,
                    'nl' : article_sortant.nbr_Barres_longitudinales,
                    'nt' : article_sortant.nbr_Barres_transversales,
                    }
        for a in article_ids:
            if e == "e1":
                localdict['pu1'] = a.poids_unit
                localdict['mc1'] = a.dimension_m2
                localdict['pl1'] = a.poids_lineaire
                localdict['d1'] = a.diametre
                localdict['dl1'] = a.diametre_longitudinal
                localdict['mt1'] = a.maille_transversal
                localdict['ml1'] = a.maille_longitudinal
                localdict['la1'] = a.largeur
                localdict['lo1'] = a.longueur
                localdict['nl1'] = a.nbr_Barres_longitudinales
                localdict['nt1'] = a.nbr_Barres_transversales
            else:
                if e == "e2":
                    localdict['pu2'] = a.poids_unit
                    localdict['mc2'] = a.dimension_m2
                    localdict['pl2'] = a.poids_lineaire
                    localdict['d2'] = a.diametre
                    localdict['dl2'] = a.diametre_longitudinal
                    localdict['mt2'] = a.maille_transversal
                    localdict['ml2'] = a.maille_longitudinal
                    localdict['la2'] = a.largeur
                    localdict['lo2'] = a.longueur
                    localdict['nl2'] = a.nbr_Barres_longitudinales
                    localdict['nt2'] = a.nbr_Barres_transversales
            valide = True
            for c in conditions:
                try:
                    if eval(c.condition, localdict) != True:
                        valide = False
                        break
                except:
                    pass
            if valide == True:
                res_article_ids.append(a.id)

        return res_article_ids

    @api.onchange('article_sortant')
    def onchange_article_sortant(self):
        self.code_machine = []
        self.article_entree1 = []
        self.article_entree2 = []
        res = {}
        dictionaire = {}
        if self.article_sortant:
            #filter sur les machines selon article_sortant
            machine_ids = []
            for ligne in self.article_sortant.article_machine_ids:
                machine_ids.append(ligne.machine_id.id)
            dictionaire['code_machine'] = [('id', 'in', machine_ids)]

            #filter sur les articles entrants
            #si 2 article entrant
            if self.article_sortant.famille_id.article_entree2 == True:
                res_article_e1_ids = []
                res_article_e2_ids = []
                for param in self.article_sortant.famille_id.famille_entrant_param2_ids:
                    article_ids = self.env['production.article'].search([('famille_id', '=', param.famille_entrant1_id.id)])
                    for a_id in self.valider_condition(article_ids, param.production_condition_ids, self.article_sortant, "e1"):
                        res_article_e1_ids.append(a_id)

                    article2_ids = self.env['production.article'].search([('famille_id', '=', param.famille_entrant2_id.id)])
                    for a_id in self.valider_condition(article2_ids, param.production_condition_ids, self.article_sortant, "e2"):
                        res_article_e2_ids.append(a_id)

                dictionaire['article_entree1'] = [('id', 'in', res_article_e1_ids)]
                dictionaire['article_entree2'] = [('id', 'in', res_article_e2_ids)]


            #un seul article entrant
            else:
                res_article_e1_ids = []
                for param in self.article_sortant.famille_id.famille_entrant_param_ids:
                    article_ids = self.env['production.article'].search([('famille_id', '=', param.famille_entrant1_id.id)])
                    for a_id in self.valider_condition(article_ids, param.production_condition_ids, self.article_sortant, "e1"):
                        res_article_e1_ids.append(a_id)

                dictionaire['article_entree1'] = [('id', 'in', res_article_e1_ids)]

        else:
            #all code_machine
            self.env.cr.execute('SELECT id FROM production_machine')
            machine_objs = self.env.cr.fetchall()
            machine_ids = []
            for m in machine_objs:
                machine_ids.append(m[0])
            dictionaire['code_machine'] = [('id', 'in', machine_ids)]

            #all article
            self.env.cr.execute('SELECT id FROM production_article')
            article_objs = self.env.cr.fetchall()
            article_ids = []
            for a in article_objs:
                article_ids.append(a[0])
            dictionaire['article_entree1'] = [('id', 'in', article_ids)]
            dictionaire['article_entree2'] = [('id', 'in', article_ids)]

        res['domain'] = dictionaire
        return res

    @api.onchange('code_machine')
    def onchange_code_machine(self):
        #on cherche la cadence a partir du relation article.machine.rel
        self.cadence = 0
        if self.article_sortant and self.code_machine:
            cadence_val = self.env['article.machine.rel'].search([('article_id', '=', self.article_sortant.id),
                                                                  ('machine_id', '=', self.code_machine.id)], 
                                                                  limit=1).cadence
            if cadence_val:
                self.cadence = cadence_val

        #filter sur les operateurs qualifier pour la machine selectionnée
        self.code_operateur = []
        res = {}
        ids = []
        if self.code_machine:
            op_ma_ids = self.env['operateur.machine.rel'].search([('machine_id', '=', self.code_machine.id)])
            for op_ma in op_ma_ids:
                ids.append(op_ma.operateur_id.id)
        else:
            self.env.cr.execute('SELECT id FROM production_operateur')
            operateur_objs = self.env.cr.fetchall()
            for op in operateur_objs:
                ids.append(op)
        res['domain'] = {'code_operateur': [('id', 'in', ids)]}
        return res

    @api.onchange('article_entree1')
    def onchange_article_entree1(self):
        res = {}
        res_article_e2_ids = []
        #si 2 articles entrants
        if self.article_sortant.famille_id.article_entree2 == True:
            #filter pour le champ article_entree2
            self.article_entree2 = []
            #article2 = self.article_entree2.id
            if self.article_sortant and self.article_entree1:
                for param in self.article_sortant.famille_id.famille_entrant_param2_ids:
                    if self.article_entree1.famille_id.id == param.famille_entrant1_id.id:
                        article2_ids = self.env['production.article'].search([('famille_id', '=', param.famille_entrant2_id.id)])
                        for a_id in self.valider_condition(article2_ids, param.production_condition_ids, self.article_sortant, "e2"):
                            res_article_e2_ids.append(a_id)

        res['domain'] = {'article_entree2': [('id', 'in', res_article_e2_ids)]}
        
        return res

#   @api.onchange('commande_id')
#   def onchange_commande_id(self):
#       res = {}
#       ids = []
#       if self.commande_id:
#           for article in self.commande_id.article_commande_ids:
#               ids.append(article.id)
#       else:
#           self.env.cr.execute('SELECT id FROM production_article')
#           article_objs = self.env.cr.fetchall()
#           for a in article_objs:
#               ids.append(a)

#       res['domain'] = {'article_sortant': [('id', 'in', ids)]}        
#       return res

    @api.multi
    def ajouter_bon_entree_fabrication(self):
        bon_form = self.env.ref('production.production_bon_entree_fabrication_form_popup', False)
        return { 
                'name': _("Bon entrée fabrication"),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'production.bon.entree.fabrication',
                'views': [(bon_form.id, 'form')],
                'view_id': 'bon_form.id',
                'target': 'new',
                'context': {'default_code_of': self.id},
                }

    @api.multi
    def reserver_pour_entree1(self):
        return { 
                'name': _("Bon de réservation"),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'bon.reservation.ordre.fabrication',
                'view_id': False,
                'context': { 
                            'default_ordre_fabrication_id': self.id,
                            'default_article_id': self.article_entree1.id,
                            'default_quantite': self.quantite1,
                            'default_quantite_demande': self.quantite1,
                            },
                }

    @api.multi
    def creer_of_from_entree1(self):
        nbr_days = abs(datetime.strptime(self.date_debut, DEFAULT_SERVER_DATE_FORMAT).date() - datetime.strptime(self.date_fin, DEFAULT_SERVER_DATE_FORMAT).date())/2
        date_prevue = str(datetime.strptime(self.date_debut, DEFAULT_SERVER_DATE_FORMAT).date() + nbr_days)
    
        return { 
                'name': _("Ordre fabrication"),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'production.ordre.fabrication',
                'view_id': False,
                'context': {
                            'default_commande_id': self.commande_id.id, 
                            'default_article_sortant': self.article_entree1.id,
                            'default_quantite': self.quantite1,
                            'default_date_fin': date_prevue
                            },
                }

#   @api.multi
#   def creer_commande_entree1(self):
#       return { 
#               'name': _("Commande fournisseur"),
#               'type': 'ir.actions.act_window',
#               'view_type': 'form',
#               'view_mode': 'form',
#               'res_model': 'article.commande.fournisseur.rel',
#               'view_id': False,
#               'context': { 
#                           'default_article_id': self.article_entree1.id,
#                           'default_quantite': self.quantite1,
#                           },
#               }

    @api.multi
    def creer_demande_entree1(self):
        return { 
                'name': _("Demande d'achat"),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'demande.achat',
                'view_id': False,
                'context': { 
                            'default_article_id': self.article_entree1.id,
                            'default_quantite_demande': self.quantite1,
                            },
                }

    @api.multi
    def reserver_pour_entree2(self):
        return { 
                'name': _("Bon de réservation"),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'bon.reservation.ordre.fabrication',
                'view_id': False,
                'context': { 
                            'default_ordre_fabrication_id': self.id,
                            'default_article_id': self.article_entree2.id,
                            'default_quantite': self.quantite2,
                            'default_quantite_demande': self.quantite2,
                            },
                }

    @api.multi
    def creer_of_from_entree2(self):
        nbr_days = abs(datetime.strptime(self.date_debut, DEFAULT_SERVER_DATE_FORMAT).date() - datetime.strptime(self.date_fin, DEFAULT_SERVER_DATE_FORMAT).date())/2
        date_prevue = str(datetime.strptime(self.date_debut, DEFAULT_SERVER_DATE_FORMAT).date() + nbr_days)

        return { 
                'name': _("Ordre fabrication"),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'production.ordre.fabrication',
                'view_id': False,
                'context': {
                            'default_commande_id': self.commande_id.id, 
                            'default_article_sortant': self.article_entree2.id,
                            'default_quantite': self.quantite2,
                            'default_date_fin': date_prevue
                            },
                }

#   @api.multi
#   def creer_commande_entree2(self):
#       return { 
#               'name': _("Commande fournisseur"),
#               'type': 'ir.actions.act_window',
#               'view_type': 'form',
#               'view_mode': 'form',
#               'res_model': 'article.commande.fournisseur.rel',
#               'view_id': False,
#               'context': { 
#                           'default_article_id': self.article_entree2.id,
#                           'default_quantite': self.quantite2,
#                           },
#               }

    @api.multi
    def creer_demande_entree2(self):
        return { 
                'name': _("Demande d'achat"),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'demande.achat',
                'view_id': False,
                'context': { 
                            'default_article_id': self.article_entree2.id,
                            'default_quantite_demande': self.quantite2,
                            },
                }
