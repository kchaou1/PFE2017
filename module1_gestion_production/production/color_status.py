# -*- coding: utf-8 -*-

import sys
import openerp
from openerp import models, fields, api, _
from openerp import tools
from datetime import date
from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.modules.module import get_module_resource
import xmlrpclib
import time
from time import mktime
import timestring
import ast






class color_status(models.Model):
    _name = 'color.status'
    state = fields.Selection([('planifie', 'Planifié'),
                              ('demarre', 'Demarré'),
                              ('termine', 'Terminé'),

                              ('brouillon', 'Brouillon'),
                              ('annulee', 'Annulee'),
                              ('attente', 'En attente de réception'),
                              ('recu_partiel', 'Reçu partiellemet'),
                              ('recu_total', 'Reçu totalemet'),
                                ], 'Etat')
    objet = fields.Selection([('ordre_fabrication', 'Ordre fabrication'), 
                              ('commande_client', 'Commande client'),
                              ('commande_fournisseur', 'Commande fournisseur')], 'Objet')
    color = fields.Integer('Coulor')


class production_article_mail(models.Model):
    _name = "production.article"
    _inherit = ['production.article','mail.thread']


class production_zone_stockage(models.Model):
    _name = "production.zone.stockage"

    of_ids = fields.One2many('production.ordre.fabrication', 'zone_stockage_id', 'OF')
    name = fields.Char('Zone de stockage', required=True)