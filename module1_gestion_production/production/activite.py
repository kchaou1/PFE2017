# -*- coding: utf-8 -*-
import sys
import openerp
from openerp import models, fields, api, _
from openerp import tools
from datetime import date
from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.modules.module import get_module_resource

#----------------------------------------------------------
# production_activite
#----------------------------------------------------------
class production_activite(models.Model):

    @api.one
    @api.depends('activite_famille_entrant_ids', 'activite_famille_sortant_ids')
    def _get_article_count(self):
        for activite in self:
            ids = []
#           for famille_entrant in activite.activite_famille_entrant_ids:
#               ids.append(famille_entrant.id)
            for famille_sortant in activite.activite_famille_sortant_ids:
                ids.append(famille_sortant.id)
            if ids:
                self.article_count = self.env['production.article'].search_count([('famille_id', 'in', tuple(ids))])

    @api.one
    @api.depends('activite_famille_entrant_ids', 'activite_famille_sortant_ids')
    def _get_machine_count(self):
        for activite in self:
            ids = []
#           for famille_entrant in activite.activite_famille_entrant_ids:
#               ids.append(famille_entrant.id)
            for famille_sortant in activite.activite_famille_sortant_ids:
                ids.append(famille_sortant.id)
            if ids:
                self.machine_count = self.env['production.machine'].search_count(['|','|','|',
                                                                                  ('famille_sortie1', 'in', tuple(ids)),
                                                                                  ('famille_sortie2', 'in', tuple(ids)),
                                                                                  ('famille_sortie3', 'in', tuple(ids)),
                                                                                  ('famille_sortie4', 'in', tuple(ids))])

    _name = 'production.activite'
    _rec_name = 'nom_activite'

    image = fields.Binary("Photo")
    image_medium = fields.Binary("Medium-sized image", compute='_compute_images', inverse='_inverse_image_medium', store=True)
    image_small = fields.Binary("Small-sized image", compute='_compute_images', inverse='_inverse_image_small', store=True)
    nom_activite = fields.Char('Activité :', required=True)
    activite_famille_entrant_ids = fields.Many2many('production.famille', 'activite_famille_entrant_rel',
                                                    'activite_id', 'famille_id', 'Familles entrantes')
    activite_famille_sortant_ids = fields.Many2many('production.famille', 'activite_famille_sortant_rel',
                                                    'activite_id', 'famille_id', 'Familles sortantes')
    #calculer le nombre d'articles pour chaque activite
    article_count = fields.Integer(compute='_get_article_count', string='Articles')
    #calculer le nombre de machines pour chaque activite
    machine_count = fields.Integer(compute='_get_machine_count', string='Machines')

    @api.depends('image')
    def _compute_images(self):
        for rec in self:
            rec.image_medium = tools.image_resize_image_medium(rec.image)
            rec.image_small = tools.image_resize_image_small(rec.image)

    def _inverse_image_medium(self):
        for rec in self:
            rec.image = tools.image_resize_image_big(rec.image_medium)

    def _inverse_image_small(self):
        for rec in self:
            rec.image = tools.image_resize_image_big(rec.image_small)

#   @api.multi
#   def ajouter_article(self):
#       return { 
#               'name': _("Article"),
#               'type': 'ir.actions.act_window',
#               'view_type': 'form',
#               'view_mode': 'form',
#               'res_model': 'production.article',
#               'view_id': False,
#               'context': {'default_activite_id': self.id},
#               }

    @api.multi
    def ajouter_article(self):
        for activite in self:
            ids = []
            article_ids = []
            for famille_sortant in activite.activite_famille_sortant_ids:
                ids.append(famille_sortant.id)
            if ids:
                article_objs = self.env['production.article'].search([('famille_id', 'in', tuple(ids))])

                for article in article_objs:
                    article_ids.append(article.id)
        return { 
                'name': _("Article"),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'production.article',
                'target': 'current',
                'context': {'default_activite_id': self.id},
                'domain' : [('id','in',article_ids)],
                }

#   @api.multi
#   def ajouter_machine(self):
#       return { 
#               'name': _("Machine"),
#               'type': 'ir.actions.act_window',
#               'view_type': 'form',
#               'view_mode': 'form',
#               'res_model': 'production.machine',
#               'view_id': False,
#               'context': {'default_activite_id': self.id},
#               }

    @api.multi
    def ajouter_machine(self):
        for activite in self:
            ids = []
            machine_ids = []
            for famille_sortant in activite.activite_famille_sortant_ids:
                ids.append(famille_sortant.id)
            if ids:
                machine_objs = self.env['production.machine'].search(['|','|','|',
                                                                      ('famille_sortie1', 'in', tuple(ids)),
                                                                      ('famille_sortie2', 'in', tuple(ids)),
                                                                      ('famille_sortie3', 'in', tuple(ids)),
                                                                      ('famille_sortie4', 'in', tuple(ids))])
                for machine in machine_objs:
                    machine_ids.append(machine.id)
        return { 
                'name': _("Machine"),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'production.machine',
                'target': 'current',
                'context': {'default_activite_id': self.id},
                'domain' : [('id','in',machine_ids)],
                }


#   @api.multi
#   def ajouter_OF(self):
#       return { 
#               'name': _("OF"),
#               'type': 'ir.actions.act_window',
#               'view_type': 'form',
#               'view_mode': 'form',
#               'res_model': 'production.ordre.fabrication',
#               'view_id': False,
#               'context': {'default_activite_id': self.id},
#               }


