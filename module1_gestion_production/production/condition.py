# -*- coding: utf-8 -*-

import sys
import openerp
from openerp import models, fields, api, _
from openerp import tools
from datetime import date
from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.modules.module import get_module_resource
import xmlrpclib
import time
from time import mktime
import timestring
import ast



class production_condition(models.Model):

    _name = 'production.condition'

    param_id = fields.Many2one('production.famille.entrant.param', 'Parametre', ondelete='cascade')
    condition = fields.Char('Condition', required=True)

    entree2 = fields.Boolean('Article entrée2')
    #field boolean for famille sortant
    diametre = fields.Boolean('Diamètre (d)')
    transversal_ha = fields.Boolean('Transversal HA')
    diametre_longitudinal = fields.Boolean('Diamètre longitudinal (dl)')
    longitudinal_ha = fields.Boolean('Longitudinal HA')
    maille_transversal = fields.Boolean('Maille transversale (mt)')
    maille_longitudinal = fields.Boolean('Maille longitudinale (ml)')
    largeur = fields.Boolean('Largeur (la)')
    longueur = fields.Boolean('Longueur (lo)')
    poids_unit = fields.Boolean('Poids unitaire (pu)')
    dimension_m2 = fields.Boolean('Surface m² (mc)')
    poids_lineaire = fields.Boolean('Poids linéaire (pl)')
    #field boolean for famille entrant1
    diametre1 = fields.Boolean('Diamètre (d)')
    transversal_ha1 = fields.Boolean('Transversal HA')
    diametre_longitudinal1 = fields.Boolean('Diamètre longitudinal (dl)')
    longitudinal_ha1 = fields.Boolean('Longitudinal HA')
    maille_transversal1 = fields.Boolean('Maille transversale (mt)')
    maille_longitudinal1 = fields.Boolean('Maille longitudinale (ml)')
    largeur1 = fields.Boolean('Largeur (la)')
    longueur1 = fields.Boolean('Longueur (lo)')
    poids_unit1 = fields.Boolean('Poids unitaire (pu)')
    dimension_m21 = fields.Boolean('Surface m² (mc)')
    poids_lineaire1 = fields.Boolean('Poids linéaire (pl)')
    #field boolean for famille entrant2
    diametre2 = fields.Boolean('Diamètre (d)')
    transversal_ha2 = fields.Boolean('Transversal HA')
    diametre_longitudinal2 = fields.Boolean('Diamètre longitudinal (dl)')
    longitudinal_ha2 = fields.Boolean('Longitudinal HA')
    maille_transversal2 = fields.Boolean('Maille transversale (mt)')
    maille_longitudinal2 = fields.Boolean('Maille longitudinale (ml)')
    largeur2 = fields.Boolean('Largeur (la)')
    longueur2 = fields.Boolean('Longueur (lo)')
    poids_unit2 = fields.Boolean('Poids unitaire (pu)')
    dimension_m22 = fields.Boolean('Surface m² (mc)')
    poids_lineaire2 = fields.Boolean('Poids linéaire (pl)')