# -*- coding: utf-8 -*-

import sys
import openerp
from openerp import models, fields, api, _
from openerp import tools
from datetime import date
from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.modules.module import get_module_resource
import xmlrpclib
import time
from time import mktime
import timestring
import ast




class bon_reservation_ordre_fabrication(models.Model):

    @api.one
    @api.depends('quantite_demande', 'quantite_reserve')
    def _get_progress_reserve_demande(self):
        if self.quantite_demande > 0 and self.quantite_reserve > 0:
            self.progress_reserve_demande = self.quantite_reserve / self.quantite_demande * 100
        else:
            self.progress_reserve_demande = 0

    @api.one
    @api.depends('article_id', 'ordre_fabrication_id')
    def _get_quantite_reserve(self):
        qte = 0
        if self.article_id and self.ordre_fabrication_id:
            bon_reservation_ids = self.env['bon.reservation.ordre.fabrication'].search([
                                                        ('ordre_fabrication_id', '=', self.ordre_fabrication_id.id), 
                                                        ('article_id', '=', self.article_id.id)])
            for br in bon_reservation_ids:
                qte += br.quantite

        self.quantite_reserve = qte

    _name = 'bon.reservation.ordre.fabrication'

    code_bon = fields.Char('Code bon :', readonly=True)
    date_bon = fields.Date('Date bon', required=True, default= lambda *a:datetime.now().strftime('%Y-%m-%d'), readonly=True)

    ordre_fabrication_id = fields.Many2one('production.ordre.fabrication', 'Code OF', ondelete='cascade', required=True,
                                    domain=[('id', 'in', [])])
    article_id = fields.Many2one('production.article', 'Code article', ondelete='cascade', required=True,
                                    domain=[('id', 'in', [])])
    quantite = fields.Float('Quantité', required=True)
    remarque = fields.Text('Remarque')

    quantite_demande = fields.Float('Quantité demandé', readonly=True)
    quantite_reserve = fields.Float(compute='_get_quantite_reserve', string='Quantité réservé')
    stock_disponible = fields.Float('Stock disponible', related='article_id.stock_disponible')
    stock_non_reserve = fields.Float('Stock non réservé', related='article_id.stock_non_reserve')

    unite = fields.Selection([('u','U'),
                              ('kg','Kg'),
                              ('m2','m²'),
                              ('m','m')], related='article_id.unite', readonly=True, string='Unite')
    progress_reserve_demande = fields.Float(compute='_get_progress_reserve_demande', string='Progression quantité réservé')
    
    etat = fields.Selection([('satisfait','Satisfait'),
                              ('nonsatisfait','Non Satisfait')]
                              , 'Etat',  default='satisfait')

    @api.model
    def create(self, values):
        #test si quantite <= 0 on genere exception
        if values['quantite'] <= 0:
            raise Warning(_('Erreur!'), 
                        _('La quantité doit étre supérieur strictement à zero ( %s )')% (values['quantite']))

        #test si quantite à réservé > stock_non_réservé ==> exception
        article_obj = self.env['production.article'].browse(values['article_id'])
        if values['quantite'] > article_obj.stock_non_reserve:
           values['etat']='nonsatisfait'

            #raise Warning(_('Erreur!'), 
                        #_('La quantité à réservé est supérieur à la quantité stock disponible'))

        #augmenter le stock_reserve
        article_obj.stock_reserve += values['quantite']

        #generer code sequence "code_bon"
        values['code_bon'] = self.env['ir.sequence'].get('bon.reservation.of')

        new_id = super(bon_reservation_ordre_fabrication, self).create(values)

        return new_id

    @api.multi
    def write(self, values):
        article_obj = self.env['production.article'].browse(self.article_id.id)
        nouv_quantite = values.get('quantite', None)
        if nouv_quantite:
            #test si quantite <= 0 on genere exception
            if nouv_quantite <= 0:
                raise Warning(_('Erreur!'), 
                            _('La quantité doit étre supérieur strictement à zero ( %s )')% (nouv_quantite))

            #test si quantite à réservé > stock_non_réservé ==> exception
            if (nouv_quantite - self.quantite) > article_obj.stock_non_reserve:
                raise Warning(_('Erreur!'), 
                            _('La quantité à réservé est supérieur à la quantité stock disponible'))

            #modifier le stock_reserve
            article_obj.stock_reserve += nouv_quantite - self.quantite

        obj_id=super(bon_reservation_ordre_fabrication, self).write(values)

        return obj_id

    @api.multi
    def unlink(self):
        for rec in self:
            article_obj = self.env['production.article'].browse(rec.article_id.id)
            #retirer la qte stock reserve
            article_obj.stock_reserve -= rec.quantite

        return super(bon_reservation_ordre_fabrication, self).unlink()

