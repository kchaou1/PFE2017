# -*- coding: utf-8 -*-

import sys
import openerp
from openerp import models, fields, api, _
from openerp import tools
from datetime import date
from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.modules.module import get_module_resource
import xmlrpclib
import time
from time import mktime
import timestring
import ast




class production_famille(models.Model):

    _name = 'production.famille'
    _rec_name = 'catdescription'

    image = fields.Binary("Photo")
    image_medium = fields.Binary("Medium-sized image", compute='_compute_images', inverse='_inverse_image_medium', store=True)
    image_small = fields.Binary("Small-sized image", compute='_compute_images', inverse='_inverse_image_small', store=True)
    article_ids = fields.One2many('production.article', 'famille_id', 'Article')
    categoryID = fields.Char('Code :',  readonly=True)
    catdescription = fields.Char('Nom', required=True)
    unite = fields.Selection([('u','U'),
                              ('kg','Kg'),
                              ('m2','m²'),
                              ('m','m')], 'Unité par Default', required=True, default='kg')
    ###parametrage interface article
    #choix des champs actives
    all_ar = fields.Boolean('Tout')
    diametre = fields.Boolean('Diamètre (d)')
    transversal_ha = fields.Boolean('Transversal HA (tha)')
    diametre_longitudinal = fields.Boolean('Diamètre longitudinal (dl)')
    longitudinal_ha = fields.Boolean('Longitudinal HA (lha)')
    maille_transversal = fields.Boolean('Maille transversale (mt)')
    maille_longitudinal = fields.Boolean('Maille longitudinale (ml)')
    largeur = fields.Boolean('Largeur (la)')
    longueur = fields.Boolean('Longueur (lo)')
    poids_unit = fields.Boolean('Poids unitaire (pu)')
    dimension_m2 = fields.Boolean('Surface m² (mc)')
    poids_lineaire = fields.Boolean('Poids linéaire (pl)')
    #calcul du poids et surface
    calcul_poids = fields.Char('Calcul poids (Kg/U) =')
    calcul_surface = fields.Char('Calcul surface (m²/U) =')
    calcul_poids_lineaire = fields.Char('Calcul poids linéaire (Kg/m) =')

    ###parametrage interface OF
    #choix des champs actives
    all_of = fields.Boolean('Tout')
    article_entree2 = fields.Boolean('Article entrée2')
    tolerance_qte_plus = fields.Boolean('Tolérance qte plus')
    tolerance_qte_moins = fields.Boolean('Tolérance qte moins')
    tolerance_dimension = fields.Boolean('Tolérance dimension')
    #parametrage des familles entrants calcul q1 et q2 , ajout des condisions
    famille_entrant_param_ids = fields.One2many('production.famille.entrant.param', 'famille_sortant_id', 'Parametre')
    famille_entrant_param2_ids = fields.One2many('production.famille.entrant.param', 'famille_sortant_id', 'Parametre')


    @api.model
    def create(self, values):
        #generer code sequence "categoryID"
        values['categoryID'] = self.env['ir.sequence'].get('production.famille')

        return super(production_famille, self).create(values)

    @api.multi
    def write(self, values):
        obj_id=super(production_famille, self).write(values)
        for obj in self:
            #test unicite des lignes params
            list_famille = []
            if obj.article_entree2 == False:
                for param in obj.famille_entrant_param_ids:
                    if param.famille_entrant1_id.id in list_famille:
                        raise Warning(_('Erreur!'), 
                                    _('Famille existe 2 fois'))
                    list_famille.append(param.famille_entrant1_id.id)
            else:
                for param in obj.famille_entrant_param2_ids:
                    if (param.famille_entrant1_id.id, param.famille_entrant2_id.id) in list_famille:
                        raise Warning(_('Erreur!'), 
                                    _('Famille existe 2 fois'))
                    list_famille.append((param.famille_entrant1_id.id, param.famille_entrant2_id.id))
            #mise à jour poids_unit dimension_m2 poids_lineaire dans article
            for articles in self.env['production.article'].search([('famille_id', '=', obj.id)]):
                articles.onchange_fields_article()

        return obj_id

    @api.depends('image')
    def _compute_images(self):
        for rec in self:
            rec.image_medium = tools.image_resize_image_medium(rec.image)
            rec.image_small = tools.image_resize_image_small(rec.image)

    def _inverse_image_medium(self):
        for rec in self:
            rec.image = tools.image_resize_image_big(rec.image_medium)

    def _inverse_image_small(self):
        for rec in self:
            rec.image = tools.image_resize_image_big(rec.image_small)

    @api.onchange('all_ar')
    def onchange_all_ar(self):
        if self.all_ar == False:
            self.diametre = False   
            self.transversal_ha = False 
            self.diametre_longitudinal = False  
            self.longitudinal_ha = False    
            self.maille_transversal = False 
            self.maille_longitudinal = False    
            self.largeur = False    
            self.longueur = False   
            self.poids_unit = False 
            self.dimension_m2 = False   
            self.poids_lineaire = False 
        else:
            if self.all_ar == True:
                self.diametre = True    
                self.transversal_ha = True  
                self.diametre_longitudinal = True   
                self.longitudinal_ha = True 
                self.maille_transversal = True  
                self.maille_longitudinal = True 
                self.largeur = True 
                self.longueur = True    
                self.poids_unit = True  
                self.dimension_m2 = True    
                self.poids_lineaire = True  

    @api.onchange('all_of')
    def onchange_all_of(self):
        if self.all_of == False:
            self.article_entree2 = False
            self.tolerance_qte_plus = False
            self.tolerance_qte_moins = False
            self.tolerance_dimension = False
        else:
            if self.all_of == True:
                self.article_entree2 = True
                self.tolerance_qte_plus = True
                self.tolerance_qte_moins = True
                self.tolerance_dimension = True
#####

    @api.multi
    def ajouter_of(self):

        return { 
                'name': _("OF"),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'production.ordre.fabrication',
                'view_id': False,
                'context': {'default_famille_id': self.id,},
                }

    @api.multi
    def ajouter_article(self):

        return { 
                'name': _("Article"),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'production.article',
                'view_id': False,
                'context': {'default_famille_id': self.id,},
                }

#----------------------------------------------------------
# production_famille_entrant_param
#----------------------------------------------------------
class production_famille_entrant_param(models.Model):

    @api.model
    def _get_unite_sortant(self):
        if self.env.context.get('default_famille_sortant_id', False):
            obj = self.env['production.famille'].browse(self.env.context['default_famille_sortant_id'])
            return obj.unite
        return False

    _name = 'production.famille.entrant.param'

    famille_sortant_id = fields.Many2one('production.famille', 'Famille sortante', ondelete='cascade', 
                                            default= lambda self: self._context.get('famille_sortant_id', False))
    unite_sortant = fields.Selection([('u','U'),
                                      ('kg','Kg'),
                                      ('m2','m²'),
                                      ('m','m')], related='famille_sortant_id.unite')
    unite_qs = fields.Selection([('u','U'),
                                 ('kg','Kg'),
                                 ('m2','m²'),
                                 ('m','m')], 'Quantité sortante (qs) ', default=_get_unite_sortant)
    famille_entrant1_id = fields.Many2one('production.famille', 'Famille entrante1', ondelete='cascade')
    unite_entrant1 = fields.Selection([('u','U'),
                                       ('kg','Kg'),
                                       ('m2','m²'),
                                       ('m','m')], related='famille_entrant1_id.unite')
    famille_entrant2_id = fields.Many2one('production.famille', 'Famille entrante2', ondelete='cascade')
    unite_entrant2 = fields.Selection([('u','U'),
                                       ('kg','Kg'),
                                       ('m2','m²'),
                                       ('m','m')], related='famille_entrant2_id.unite')
    calcul_quantite1 = fields.Char('Calcul quantité1')
    unite_q1 = fields.Selection([('u','U'),
                                 ('kg','Kg'),
                                 ('m2','m²'),
                                 ('m','m')], 'Quantité entrante1 (q1) ')
    calcul_quantite2 = fields.Char('Calcul quantité2')
    unite_q2 = fields.Selection([('u','U'),
                                 ('kg','Kg'),
                                 ('m2','m²'),
                                 ('m','m')], 'Quantité entrante2 (q2) ')

    production_condition_ids = fields.One2many('production.condition', 'param_id', 'Conditions')
    #field boolean for 2eme article entrant
    entree2 = fields.Boolean('Article entrée2', related='famille_sortant_id.article_entree2')
    #field boolean for famille sortant
    diametre = fields.Boolean('Diamètre (d)', related='famille_sortant_id.diametre')
    transversal_ha = fields.Boolean('Transversal HA', related='famille_sortant_id.transversal_ha')
    diametre_longitudinal = fields.Boolean('Diamètre longitudinal (dl)', related='famille_sortant_id.diametre_longitudinal')
    longitudinal_ha = fields.Boolean('Longitudinal HA', related='famille_sortant_id.longitudinal_ha')
    maille_transversal = fields.Boolean('Maille transversale (mt)', related='famille_sortant_id.maille_transversal')
    maille_longitudinal = fields.Boolean('Maille longitudinale (ml)', related='famille_sortant_id.maille_longitudinal')
    largeur = fields.Boolean('Largeur (la)', related='famille_sortant_id.largeur')
    longueur = fields.Boolean('Longueur (lo)', related='famille_sortant_id.longueur')
    poids_unit = fields.Boolean('Poids unitaire (pu)', related='famille_sortant_id.poids_unit')
    dimension_m2 = fields.Boolean('Surface m² (mc)', related='famille_sortant_id.dimension_m2')
    poids_lineaire = fields.Boolean('Poids linéaire (pl)', related='famille_sortant_id.poids_lineaire')
    #field boolean for famille entrant1
    diametre1 = fields.Boolean('Diamètre (d)', related='famille_entrant1_id.diametre')
    transversal_ha1 = fields.Boolean('Transversal HA', related='famille_entrant1_id.transversal_ha')
    diametre_longitudinal1 = fields.Boolean('Diamètre longitudinal (dl)', related='famille_entrant1_id.diametre_longitudinal')
    longitudinal_ha1 = fields.Boolean('Longitudinal HA', related='famille_entrant1_id.longitudinal_ha')
    maille_transversal1 = fields.Boolean('Maille transversale (mt)', related='famille_entrant1_id.maille_transversal')
    maille_longitudinal1 = fields.Boolean('Maille longitudinale (ml)', related='famille_entrant1_id.maille_longitudinal')
    largeur1 = fields.Boolean('Largeur (la)', related='famille_entrant1_id.largeur')
    longueur1 = fields.Boolean('Longueur (lo)', related='famille_entrant1_id.longueur')
    poids_unit1 = fields.Boolean('Poids unitaire (pu)', related='famille_entrant1_id.poids_unit')
    dimension_m21 = fields.Boolean('Surface m² (mc)', related='famille_entrant1_id.dimension_m2')
    poids_lineaire1 = fields.Boolean('Poids linéaire (pl)', related='famille_entrant1_id.poids_lineaire')
    #field boolean for famille entrant2
    diametre2 = fields.Boolean('Diamètre (d)', related='famille_entrant2_id.diametre')
    transversal_ha2 = fields.Boolean('Transversal HA', related='famille_entrant2_id.transversal_ha')
    diametre_longitudinal2 = fields.Boolean('Diamètre longitudinal (dl)', related='famille_entrant2_id.diametre_longitudinal')
    longitudinal_ha2 = fields.Boolean('Longitudinal HA', related='famille_entrant2_id.longitudinal_ha')
    maille_transversal2 = fields.Boolean('Maille transversale (mt)', related='famille_entrant2_id.maille_transversal')
    maille_longitudinal2 = fields.Boolean('Maille longitudinale (ml)', related='famille_entrant2_id.maille_longitudinal')
    largeur2 = fields.Boolean('Largeur (la)', related='famille_entrant2_id.largeur')
    longueur2 = fields.Boolean('Longueur (lo)', related='famille_entrant2_id.longueur')
    poids_unit2 = fields.Boolean('Poids unitaire (pu)', related='famille_entrant2_id.poids_unit')
    dimension_m22 = fields.Boolean('Surface m² (mc)', related='famille_entrant2_id.dimension_m2')
    poids_lineaire2 = fields.Boolean('Poids linéaire (pl)', related='famille_entrant2_id.poids_lineaire')

    @api.onchange('famille_sortant_id')
    def onchange_famille_sortant(self):
        #filter sur le champ Famille entrant1 et Famille entrant2
        self.famille_entrant1_id = []
        self.famille_entrant2_id = []
        res = {}
        a_ids = []
        ids = []
        if self._context.get('default_famille_sortant_id', False):
            self.env.cr.execute('SELECT activite_id FROM activite_famille_sortant_rel where famille_id = %s', 
                                (self._context.get('default_famille_sortant_id', False),))
            activite_ids = self.env.cr.fetchall()
            for a in activite_ids:
                a_ids.append(a)
            if a_ids:
                self.env.cr.execute('SELECT famille_id FROM activite_famille_entrant_rel where activite_id IN %s', 
                                    (tuple(a_ids),))
                famille_ids = self.env.cr.fetchall()
                for f in famille_ids:
                    ids.append(f)
        res['domain'] = {
                        'famille_entrant1_id': [('id', 'in', ids)],
                        'famille_entrant2_id': [('id', 'in', ids)]
                        }
        return res

    @api.onchange('famille_entrant1_id')
    def onchange_famille_entrant1_id(self):
        self.unite_q1 = self.famille_entrant1_id.unite

    @api.onchange('famille_entrant2_id')
    def onchange_famille_entrant2_id(self):
        self.unite_q2 = self.famille_entrant2_id.unite

    @api.multi
    def validate_formule(self, formule):
        if formule == "qs":
            return True
        return False

    @api.multi
    def test_formule(self, formule, values):
        localdict = {}
        localdict['qs'] = 1
        localdict['q1'] = 1
        localdict['q2'] = 1

        if values.get("diametre", None) == True or self.diametre == True: 
            localdict['d'] = 1
        if values.get("diametre_longitudinal", None) == True or self.diametre_longitudinal == True:
            localdict['dl'] = 1
        if values.get("maille_transversal", None) == True or self.maille_transversal == True:
            localdict['mt'] = 1
        if values.get("maille_longitudinal", None) == True or self.maille_longitudinal == True:
            localdict['ml'] = 1
        if values.get("largeur", None) == True or self.largeur == True:
            localdict['la'] = 1
        if values.get("longueur", None) == True or self.longueur == True:
            localdict['lo'] = 1
        if values.get("largeur", None) == True and values.get("maille_transversal", None) == True  or self.largeur == True and self.maille_transversal == True:
            localdict['nl'] = 1
        if values.get("longueur", None) == True and values.get("maille_longitudinal", None) == True or self.longueur == True and self.maille_longitudinal == True:
            localdict['nt'] = 1
        if values.get("poids_unit", None) == True or self.poids_unit == True:
            localdict['pu'] = 1
        if values.get("dimension_m2", None) == True or self.dimension_m2 == True:
            localdict['mc'] = 1
        if values.get("poids_lineaire", None) == True or self.poids_lineaire == True:
            localdict['pl'] = 1

        if values.get("poids_unit1", None) == True or self.poids_unit1 == True:
            localdict['pu1'] = 1
        if values.get("dimension_m21", None) == True or self.dimension_m21 == True:
            localdict['mc1'] = 1
        if values.get("poids_lineaire1", None) == True or self.poids_lineaire1 == True:
            localdict['pl1'] = 1
        if values.get("diametre1", None) == True or self.diametre1 == True:
            localdict['d1'] = 1
        if values.get("diametre_longitudinal1", None) == True or self.diametre_longitudinal1 == True:
            localdict['dl1'] = 1
        if values.get("maille_transversal1", None) == True or self.maille_transversal1 == True:
            localdict['mt1'] = 1
        if values.get("maille_longitudinal1", None) == True or self.maille_longitudinal1 == True:
            localdict['ml1'] = 1
        if values.get("largeur1", None) == True or self.largeur1 == True:
            localdict['la1'] = 1
        if values.get("longueur1", None) == True or self.longueur1 == True:
            localdict['lo1'] = 1
        if values.get("largeur1", None) == True and values.get("maille_transversal1", None) == True or self.largeur1 == True and self.maille_transversal1 == True:
            localdict['nl1'] = 1
        if values.get("longueur1", None) == True and values.get("maille_longitudinal1", None) == True or self.longueur1 == True and self.maille_longitudinal1 == True:
            localdict['nt1'] = 1

        if values.get("poids_unit2", None) == True or self.poids_unit2 == True:
            localdict['pu2'] = 1
        if values.get("dimension_m22", None) == True or self.dimension_m22 == True:
            localdict['mc2'] = 1
        if values.get("poids_lineaire2", None) == True or self.poids_lineaire2 == True:
            localdict['pl2'] = 1
        if values.get("diametre2", None) == True or self.diametre2 == True:
            localdict['d2'] = 1
        if values.get("diametre_longitudinal2", None) == True or self.diametre_longitudinal2 == True:
            localdict['dl2'] = 1
        if values.get("maille_transversal2", None) == True or self.maille_transversal2 == True:
            localdict['mt2'] = 1
        if values.get("maille_longitudinal2", None) == True or self.maille_longitudinal2 == True:
            localdict['ml2'] = 1
        if values.get("largeur2", None) == True or self.largeur2 == True:
            localdict['la2'] = 1
        if values.get("longueur2", None) == True or self.longueur2 == True:
            localdict['lo2'] = 1
        if values.get("largeur2", None) == True and values.get("maille_transversal2", None) == True or self.largeur2 == True and self.maille_transversal2 == True:
            localdict['nl2'] = 1
        if values.get("longueur2", None) == True and values.get("maille_longitudinal2", None) == True or self.longueur2 == True  and self.maille_longitudinal2 == True:
            localdict['nt2'] = 1

        try:
            eval(formule, localdict)
            return True
        except:
            return False

    @api.model
    def create(self, values):
        #test formule1
        formule1 = values.get("calcul_quantite1", None)
        if formule1:
            if self.test_formule(formule1, values) == False:
                raise osv.except_osv(_('Erreur!'), _('Formule invalide [ %s ].')% (formule1))

        #test conditions
        conditions = values.get("production_condition_ids", None)
        for line in conditions:
            if line[2]:
                condition = line[2].get("condition", None)
                if condition:
                    if self.test_formule(condition, values) == False:
                        raise Warning(_('Erreur!'), 
                                    _('Condition invalide [ %s ].')% (condition))


        #test formule2
        formule2 = values.get("calcul_quantite2", None)
        if formule2:
            if self.test_formule(formule2, values) == False:
                raise Warning(_('Erreur!'), 
                            _('Formule invalide [ %s ].')% (formule2))

        obj_id = super(production_famille_entrant_param, self).create(values)

        return obj_id

    @api.multi
    def write(self,values):
        #test formule1
        formule1 = values.get("calcul_quantite1", None)
        if formule1:
            if self.test_formule(formule1, values) == False:
                raise Warning(_('Erreur!'), 
                            _('Formule invalide [ %s ].')% (formule1))

        #test conditions
        conditions = values.get("production_condition_ids", None)
        if conditions:
            for line in conditions:
                if line[2]:
                    condition = line[2].get("condition", None)
                    if condition:
                        if self.test_formule(condition, values) == False:
                            raise Warning(_('Erreur!'), 
                                        _('Condition invalide [ %s ].')% (condition))

        #test formule2
        formule2 = values.get("calcul_quantite2", None)
        if formule2:
            if self.test_formule(formule2, values) == False:
                raise Warning(_('Erreur!'), 
                            _('Formule invalide [ %s ].')% (formule2))

        obj_id = super(production_famille_entrant_param, self).write(values)
        return obj_id