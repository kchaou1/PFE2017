# -*- coding: utf-8 -*-

import sys
import openerp
from openerp import models, fields, api, _
from openerp import tools
from datetime import date
from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.modules.module import get_module_resource
import xmlrpclib
import time
from time import mktime
import timestring
import ast








class production_machine(models.Model):

    @api.multi
    @api.depends('nom', 'code_machine')
    def name_get(self):
        result = []
        for record in self:
            result.append((record.id, record.nom + ' (' + record.code_machine + ')'))
        return result

    @api.onchange('activite_id')
    def onchange_activite(self):
        self.famille_id = []
        res = {}
        ids_entrant = []
        ids_sortant = []
        if self.activite_id:
            for famille_sortant in self.activite_id.activite_famille_sortant_ids:
                ids_sortant.append(famille_sortant.id)
            for famille_entrant in self.activite_id.activite_famille_entrant_ids:
                ids_entrant.append(famille_entrant.id)
        else:
            self.env.cr.execute('SELECT id FROM production_famille')
            famille_objs = self.env.cr.fetchall()
            for f in famille_objs:
                ids_entrant.append(f)
                ids_sortant.append(f)
        res['domain'] = {
                        'famille_entree1': [('id', 'in', ids_entrant)],
                        'famille_entree2': [('id', 'in', ids_entrant)],
                        'famille_entree3': [('id', 'in', ids_entrant)],
                        'famille_entree4': [('id', 'in', ids_entrant)],
                        'famille_sortie1': [('id', 'in', ids_sortant)],
                        'famille_sortie2': [('id', 'in', ids_sortant)],
                        'famille_sortie3': [('id', 'in', ids_sortant)],
                        'famille_sortie4': [('id', 'in', ids_sortant)]
                        }
        return res

    @api.one
    def _get_duree_fonctionnement(self):
        duree = 0
        for m in self:
            for of in self.env['production.ordre.fabrication'].search([('code_machine', '=', m.id)]):
                for bef in of.bon_entree_fabrication_ids:
                    duree += bef.duree
        self.duree_fonctionnement = duree

    #calcul temp d'arrêt par machine
    @api.one
    def _get_temp_arret(self):
        temp = 0

        for m in self:
            for di in self.env['maintenance.demande.intervention'].search([('machine_id', '=', m.id)]):
                temp += di.temp_arret
        self.temp_arret = temp

    @api.one
    def _get_piece_count(self):
        for m in self:
            self.piece_count  = self.env['machine.piece.rel'].search_count([('machine_id', '=', m.id)])

    @api.one
    def _get_demande_intervention_count(self):
        for m in self:
            self.demande_intervention_count  = self.env['maintenance.demande.intervention'].search_count([('machine_id', '=', m.id)])

    @api.one
    def _get_maintenance_preventive_count(self):
        for m in self:
            self.maintenance_preventive_count  = self.env['maintenance.preventive'].search_count([('machine_id', '=', m.id)])

    _name = 'production.machine'

    image = fields.Binary("Photo")
    image_medium = fields.Binary("Medium-sized image", compute='_compute_images', inverse='_inverse_image_medium', store=True)
    image_small = fields.Binary("Small-sized image", compute='_compute_images', inverse='_inverse_image_small', store=True)
    activite_id = fields.Many2one('production.activite', 'Activité', ondelete='cascade')
    code_machine = fields.Char('Code', required=True)
    nom = fields.Char('Nom', required=True)
    annee_debut_production = fields.Selection([(num, str(num)) for num in range(1990, (datetime.now().year)+1 )], 'Année',      
                                                default= lambda *a:datetime.now().year)
    famille_entree1 = fields.Many2one('production.famille', 'Famille entrée1', ondelete='cascade')
    famille_entree2 = fields.Many2one('production.famille', 'Famille entrée2', ondelete='cascade')
    famille_entree3 = fields.Many2one('production.famille', 'Famille entrée3', ondelete='cascade')
    famille_entree4 = fields.Many2one('production.famille', 'Famille entrée4', ondelete='cascade')
    famille_sortie1 = fields.Many2one('production.famille', 'Famille sortie1', ondelete='cascade')
    famille_sortie2 = fields.Many2one('production.famille', 'Famille sortie2', ondelete='cascade')
    famille_sortie3 = fields.Many2one('production.famille', 'Famille sortie3', ondelete='cascade')
    famille_sortie4 = fields.Many2one('production.famille', 'Famille sortie4', ondelete='cascade')
    active = fields.Boolean('Active', default=True)

    duree_fonctionnement = fields.Float(compute='_get_duree_fonctionnement', string="Durée de fonctionnement")
    temp_arret = fields.Float(compute='_get_temp_arret', string="Temp d'arrêt")

    machine_piece_ids = fields.Many2many('maintenance.piece', 'machine_piece_rel', 'machine_id', 'piece_id', 'Piéces')
    demande_intervention_ids = fields.One2many('maintenance.demande.intervention', 'machine_id', 'Interventions')
    maintenance_preventive_ids = fields.One2many('maintenance.preventive', 'machine_id', 'Maintenance préventive')

    piece_count = fields.Integer(compute='_get_piece_count', string='Pièces')
    demande_intervention_count = fields.Integer(compute='_get_demande_intervention_count', string='Interventions')
    maintenance_preventive_count = fields.Integer(compute='_get_maintenance_preventive_count', string='M.P')

    operateur_machine_ids = fields.Many2many('production.operateur', 'operateur_machine_rel', 'machine_id', 'operateur_id',
                                                'Qualifié pour les machines')
    @api.model
    def create(self, values):
        #test code_machine doit etre unique
        if self.env['production.machine'].search_count([('code_machine', '=', values['code_machine'])]) > 0:
            raise Warning(_('Erreur!'), 
                        _('Code machine existe déjà [ %s ].')% (values['code_machine']))
        new_id = super(production_machine, self).create(values)
        #vals{'':}
        #self.pool.get('production.machine').create(values)
        return new_id
   
    @api.multi
    def write(self, values):
        obj_id=super(production_machine,self).write(values)
        #test code_machine doit etre unique
        if self.env['production.machine'].search_count([('code_machine', '=', self.code_machine)]) > 1:
            raise Warning(_('Erreur!'), 
                        _('Code machine existe déjà [ %s ].')% (self.code_machine))
        return obj_id

    @api.depends('image')
    def _compute_images(self):
        for rec in self:
            rec.image_medium = tools.image_resize_image_medium(rec.image)
            rec.image_small = tools.image_resize_image_small(rec.image)

    def _inverse_image_medium(self):
        for rec in self:
            rec.image = tools.image_resize_image_big(rec.image_medium)

    def _inverse_image_small(self):
        for rec in self:
            rec.image = tools.image_resize_image_big(rec.image_small)

    @api.multi
    def ajouter_piece(self):
        return { 
                'name': _("Piéce"),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'maintenance.piece',
                'view_id': False,
                'context': {'default_machine_id': self.id,},
                }

    @api.multi
    def ajouter_maintenance_preventive(self):
        return { 
                'name': _("Maintenance preventive"),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'maintenance.preventive',
                'view_id': False,
                'context': {'default_machine_id': self.id},
                }

    @api.multi
    def ajouter_demande_intervention(self):
        return { 
                'name': _("Demande intervention"),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'maintenance.demande.intervention',
                'view_id': False,
                'context': {'default_machine_id': self.id},
                }
    
    @api.multi
    def creer_of(self):
        print self.id
        return { 
                'name': _("OF"),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'production.ordre.fabrication',
                'view_id': False,
                'context': {
                'default_activite_id':self.activite_id.id,
                'default_famille_id':self.famille_sortie1.id,
                'default_code_machine':self.id,

                
                },
                }