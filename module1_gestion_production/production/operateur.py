# -*- coding: utf-8 -*-

import sys
import openerp
from openerp import models, fields, api, _
from openerp import tools
from datetime import date
from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.modules.module import get_module_resource
import xmlrpclib
import time
from time import mktime
import timestring
import ast





class production_operateur(models.Model):

    @api.multi
    @api.depends('prenom', 'nom', 'code_operateur')
    def name_get(self):
        result = []
        for record in self:
#           result.append((record.id, record.prenom + ' ' + record.nom + ' (' + record.code_operateur +')'))
            note_moyenne = str(record.note_moyenne) if record.note_moyenne != False else ''
            result.append((record.id, record.prenom + ' ' + record.nom + ' ( ' + note_moyenne +' /20)'))
        return result

    @api.one
    def _get_note_moyenne(self):
        total = 0.0
        con = 0.0
        for m in self:
            for e in m.evaluation_ids:
                if e.qualite_service == "0":
                    total += 0
                elif e.qualite_service == "1":
                    total += 5
                elif e.qualite_service == "2":
                    total += 10
                elif e.qualite_service == "3":
                    total += 15
                elif e.qualite_service == "4":
                    total += 20
                else:
                    total += 0
                con += 1

        if con > 0:
            self.note_moyenne = total / con

    _name = 'production.operateur'

    image = fields.Binary("Image")
    image_medium = fields.Binary("Medium-sized image", compute='_compute_images', inverse='_inverse_image_medium', store=True)
    image_small = fields.Binary("Small-sized image", compute='_compute_images', inverse='_inverse_image_small', store=True)
    code_operateur = fields.Char('Code opérateur :', required=True)
    nom = fields.Char('Nom', required=True)
    prenom = fields.Char('Prénom', required=True)
    operateur_machine_ids = fields.Many2many('production.machine', 'operateur_machine_rel', 'operateur_id', 'machine_id',
                                                'Qualifié pour les machines')
    tel = fields.Char('Téléphone')
    email = fields.Char('Email')
    active = fields.Boolean('Actif', default=True)

    #coût horaire visible que par admin
    cout_horaire = fields.Float('Coût horaire')
    evaluation_ids = fields.One2many('maintenance.evaluation.operateur', 'production_operateur_id', 'Evaluations')
    note_moyenne = fields.Float(compute='_get_note_moyenne', string="Note moyenne", digits=(16,1))

    @api.depends('image')
    def _compute_images(self):
        for rec in self:
            rec.image_medium = tools.image_resize_image_medium(rec.image)
            rec.image_small = tools.image_resize_image_small(rec.image)

    def _inverse_image_medium(self):
        for rec in self:
            rec.image = tools.image_resize_image_big(rec.image_medium)

    def _inverse_image_small(self):
        for rec in self:
            rec.image = tools.image_resize_image_big(rec.image_small)

    @api.model
    def create(self, values):
        #test code_operateur doit etre unique
        if self.env['production.operateur'].search_count([('code_operateur', '=', values['code_operateur'])]) > 0:
            raise Warning(_('Erreur!'), 
                        _('Code operateur existe déjà [ %s ].')% (values['code_operateur']))
        new_id = super(production_operateur, self).create(values)
        return new_id

    @api.multi
    def write(self, values):
        obj_id=super(production_operateur,self).write(values)
        #test code_operateur doit etre unique
        if self.env['production.operateur'].search_count([('code_operateur', '=', self.code_operateur)]) > 1:
            raise Warning(_('Erreur!'), 
                        _('Code operateur existe déjà [ %s ].')% (self.code_operateur))
        return obj_id