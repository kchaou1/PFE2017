# -*- coding: utf-8 -*-

import sys
import openerp
from openerp import models, fields, api, _
from openerp import tools
from datetime import date
from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.modules.module import get_module_resource
import xmlrpclib
import time
from time import mktime
import timestring
import ast

class production_bon_entree_fabrication(models.Model):

    @api.one
    @api.depends('code_of')
    def _get_quantite_restante(self):
        qte = self.code_of.quantite
        for bef in self.env['production.bon.entree.fabrication'].search([('code_of', '=', self.code_of.id)]):
            qte = qte - bef.quantite
        self.qte_restante = qte

    _name = 'production.bon.entree.fabrication'

    code_bon = fields.Char('Code bon :', readonly=True)
    code_of = fields.Many2one('production.ordre.fabrication', 'Code OF', ondelete='cascade', required=True, 
                                domain=[('state','=','demarre')])
    quantite = fields.Float('Quantité', required=True)
    duree = fields.Float('Durée' , required=True)
    date_bon = fields.Date('Date bon' , required=True, default= lambda *a:datetime.now().strftime('%Y-%m-%d'))
    unite = fields.Selection([('u','U'),
                              ('kg','Kg'),
                              ('m2','m²'),
                              ('m','m')], related='code_of.unite_article_sortant')
    qte_restante = fields.Integer(compute='_get_quantite_restante', string='Quantité restante')

    @api.model
    def create(self, values):
        #test si quantite <= 0 on genere exception
        if values['quantite'] <= 0:
            raise Warning(_('Erreur!'), 
                        _('La quantité doit étre supérieur strictement à zero ( %s )')% (values['quantite']))

        #test si duree <= 0 on genere exception
        if values['duree'] <= 0:
            raise Warning(_('Erreur!'),
                        _('La duree doit étre supérieur strictement à zero'))

        #generer code sequence "code_bon"
        values['code_bon'] = self.env['ir.sequence'].get('production.bon.entree.fab')

        for of in self.env['production.ordre.fabrication'].browse(values['code_of']):
            #augmenter stock article sortant
            of.article_sortant.stock_reel += values['quantite']

            #test stock maximale
            of.article_sortant.verifier_stock()

            #Calcul du rapport : qte_realisé / qte_total
            qte_realise_pc = values['quantite'] / of.quantite
            #retirer stock article entrée1
            qte_realise1 = of.quantite1 * qte_realise_pc
            of.article_entree1.stock_reserve -= qte_realise1
            of.article_entree1.stock_reel -= qte_realise1

            if of.article_sortant.famille_id.article_entree2:#si 2 article entrant
                #retirer stock article entrée2
                qte_realise2 = of.quantite2 * qte_realise_pc
                of.article_entree2.stock_reserve -= qte_realise2
                of.article_entree2.stock_reel -= qte_realise2

        new_id = super(production_bon_entree_fabrication, self).create(values)
     
        return new_id

    @api.multi
    def write(self, values):
        nouv_quantite = values.get('quantite', None)
        nouv_of = values.get('code_of', None)
        nouv_duree = values.get('duree', None)
        #test si duree <= 0 on genere exception
        if nouv_duree:
            if nouv_duree <= 0:
                raise Warning(_('Erreur!'), 
                            _('La duree doit étre supérieur strictement à zero'))

        #test si quantite <= 0 on genere exception
        if nouv_quantite:
            if nouv_quantite <= 0:
                raise Warning(_('Erreur!'), 
                            _('La quantité doit étre supérieur strictement à zero ( %s )')% (nouv_quantite))
        if nouv_of:
            if nouv_quantite:
                #modifier stock
                for of in self.env['production.ordre.fabrication'].browse(self.code_of.id):
                    #modifier stock article sortant
                    of.article_sortant.stock_reel -= self.quantite

                    #Calcul du rapport : qte_realisé / qte_total
                    qte_realise_pc = self.quantite / of.quantite
                    #retirer stock article entrée1
                    qte_realise1 = of.quantite1 * qte_realise_pc
                    of.article_entree1.stock_reserve += qte_realise1
                    of.article_entree1.stock_reel += qte_realise1

                    if of.article_sortant.famille_id.article_entree2:#si 2 article entrant
                        #retirer stock article entrée2
                        qte_realise2 = of.quantite2 * qte_realise_pc
                        of.article_entree2.stock_reserve += qte_realise2
                        of.article_entree2.stock_reel += qte_realise2

                for of in self.env['production.ordre.fabrication'].browse(nouv_of):
                    #augmenter stock article sortant
                    of.article_sortant.stock_reel += nouv_quantite

                    #test stock maximale
                    of.article_sortant.verifier_stock()

                    #Calcul du rapport : qte_realisé / qte_total
                    qte_realise_pc = nouv_quantite / of.quantite
                    #retirer stock article entrée1
                    qte_realise1 = of.quantite1 * qte_realise_pc
                    of.article_entree1.stock_reserve -= qte_realise1
                    of.article_entree1.stock_reel -= qte_realise1

                    if of.article_sortant.famille_id.article_entree2:#si 2 article entrant
                        #retirer stock article entrée2
                        qte_realise2 = of.quantite2 * qte_realise_pc
                        of.article_entree2.stock_reserve -= qte_realise2
                        of.article_entree2.stock_reel -= qte_realise2


            else:#même quantite
                #modifier stock
                for of in self.env['production.ordre.fabrication'].browse(self.code_of.id):
                    #modifier stock article sortant
                    of.article_sortant.stock_reel -= self.quantite

                    #Calcul du rapport : qte_realisé / qte_total
                    qte_realise_pc = self.quantite / of.quantite
                    #retirer stock article entrée1
                    qte_realise1 = of.quantite1 * qte_realise_pc
                    of.article_entree1.stock_reserve += qte_realise1
                    of.article_entree1.stock_reel += qte_realise1

                    if of.article_sortant.famille_id.article_entree2:#si 2 article entrant
                        #retirer stock article entrée2
                        qte_realise2 = of.quantite2 * qte_realise_pc
                        of.article_entree2.stock_reserve += qte_realise2
                        of.article_entree2.stock_reel += qte_realise2

                for of in self.env['production.ordre.fabrication'].browse(nouv_of):
                    #augmenter stock article sortant
                    of.article_sortant.stock_reel += self.quantite

                    #test stock maximale
                    of.article_sortant.verifier_stock()

                    #Calcul du rapport : qte_realisé / qte_total
                    qte_realise_pc = self.quantite / of.quantite
                    #retirer stock article entrée1
                    qte_realise1 = of.quantite1 * qte_realise_pc
                    of.article_entree1.stock_reserve -= qte_realise1
                    of.article_entree1.stock_reel -= qte_realise1

                    if of.article_sortant.famille_id.article_entree2:#si 2 article entrant
                        #retirer stock article entrée2
                        qte_realise2 = of.quantite2 * qte_realise_pc
                        of.article_entree2.stock_reserve -= qte_realise2
                        of.article_entree2.stock_reel -= qte_realise2


        else:#même code_of
            if nouv_quantite:
                for of in self.env['production.ordre.fabrication'].browse(self.code_of.id):
                    #modifier stock article sortant
                    of.article_sortant.stock_reel += nouv_quantite - self.quantite

                    #test stock maximale
                    of.article_sortant.verifier_stock()

                    #Calcul du rapport : qte_realisé / qte_total
                    ancien_qte_realise_pc = self.quantite / of.quantite
                    nouveau_qte_realise_pc = nouv_quantite / of.quantite

                    #modifier stock article entrée1
                    ancien_qte_realise1 = of.quantite1 * ancien_qte_realise_pc
                    nouveau_qte_realise1 = of.quantite1 * nouveau_qte_realise_pc
                    of.article_entree1.stock_reserve += ancien_qte_realise1 - nouveau_qte_realise1
                    of.article_entree1.stock_reel += ancien_qte_realise1 - nouveau_qte_realise1

                    if of.article_sortant.famille_id.article_entree2:#si 2 article entrant
                        #modifier stock article entrée2
                        ancien_qte_realise2 = of.quantite2 * ancien_qte_realise_pc
                        nouveau_qte_realise2 = of.quantite2 * nouveau_qte_realise_pc
                        of.article_entree2.stock_reserve += ancien_qte_realise2 - nouveau_qte_realise2
                        of.article_entree2.stock_reel += ancien_qte_realise2 - nouveau_qte_realise2


        obj_id=super(production_bon_entree_fabrication, self).write(values)

        return obj_id

    @api.multi
    def unlink(self):
        for rec in self:
            for of in self.env['production.ordre.fabrication'].browse(rec.code_of.id):
                #modifier stock article sortant
                of.article_sortant.stock_reel -= rec.quantite

                #Calcul du rapport : qte_realisé / qte_total
                qte_realise_pc = rec.quantite / of.quantite
                #modifier stock article entrée1
                qte_realise1 = of.quantite1 * qte_realise_pc
                of.article_entree1.stock_reserve += qte_realise1
                of.article_entree1.stock_reel += qte_realise1

                if of.article_sortant.famille_id.article_entree2:#si 2 article entrant
                    #modifier stock article entrée2
                    qte_realise2 = of.quantite2 * qte_realise_pc
                    of.article_entree2.stock_reserve += qte_realise2
                    of.article_entree2.stock_reel += qte_realise2

        return super(production_bon_entree_fabrication, self).unlink()
