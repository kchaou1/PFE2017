# -*- coding: utf-8 -*-

import sys
import openerp
from openerp import models, fields, api, _
from openerp import tools
from datetime import date
from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.modules.module import get_module_resource
import xmlrpclib
import time
from time import mktime
import timestring
import ast





class operateur_machine_rel(models.Model):
    _name = "operateur.machine.rel"

    operateur_id = fields.Many2one('production.operateur', 'Operateur', ondelete='cascade')
    machine_id = fields.Many2one('production.machine', 'Machine', ondelete='cascade')