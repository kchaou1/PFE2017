# -*- coding: utf-8 -*-

import sys
import openerp
from openerp import models, fields, api, _
from openerp import tools
from datetime import date
from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.modules.module import get_module_resource
import xmlrpclib
import time
from time import mktime
import timestring
import ast










class production_article(models.Model):

    @api.onchange('famille_id')
    def onchange_famille(self):
        self.unite = self.famille_id.unite
        #si on change la famille on vide la liste des machines
        self.article_machine_ids = []

#   @api.onchange('activite_id')
#   def onchange_activite(self):
#       self.famille_id = []
#       res = {}
#       ids = []
#       if self.activite_id:
#           #on cherche les familles sortants de l'activité selectionné
#           for famille_sortant in self.activite_id.activite_famille_sortant_ids:
#               ids.append(famille_sortant.id)
#       else:
#           self.env.cr.execute('SELECT id FROM production_famille')
#           famille_objs = self.env.cr.fetchall()
#           for f in famille_objs:
#               ids.append(f)
#       res['domain'] = {'famille_id': [('id', 'in', ids)]}
#       return res

    @api.onchange('largeur','maille_transversal')
    def onchange_largeur_ou_mt(self):
        if self.largeur > 0 and self.maille_transversal > 0 :
            self.nbr_Barres_longitudinales = int(self.largeur / self.maille_transversal)

    @api.onchange('longueur','maille_longitudinal')
    def onchange_longueur_ou_ml(self):
        if self.longueur > 0 and self.maille_longitudinal > 0 :
            self.nbr_Barres_transversales = int(self.longueur / self.maille_longitudinal)

    @api.onchange('diametre','diametre_longitudinal','maille_transversal','maille_longitudinal','largeur','longueur',
                    'nbr_Barres_longitudinales','nbr_Barres_transversales')
    def onchange_fields_article(self):
        localdict = {
                    'd' : self.diametre,
                    'dl' : self.diametre_longitudinal,
                    'mt' : self.maille_transversal,
                    'ml' : self.maille_longitudinal,
                    'la' : self.largeur,
                    'lo' : self.longueur,
                    'nl' : self.nbr_Barres_longitudinales,
                    'nt' : self.nbr_Barres_transversales,
                    'pu' : self.poids_unit,
                    'mc' : self.dimension_m2,
                    'pl' : self.poids_lineaire,
                    }

        #calcul du Poids unit 
        if self.famille_id.calcul_poids:
            try:
                localdict['pu'] = eval(self.famille_id.calcul_poids, localdict)
                self.poids_unit = localdict.get("pu", None)
            except:
                pass

        #calcul du Dimension m² 
        if self.famille_id.calcul_surface:
            try:
                localdict['mc'] = eval(self.famille_id.calcul_surface, localdict)
                self.dimension_m2 = localdict.get("mc", None)
            except:
                pass

        #calcul du Poids linéaire
        if self.famille_id.calcul_poids_lineaire:
            try:
                localdict['pl'] = eval(self.famille_id.calcul_poids_lineaire, localdict)
                self.poids_lineaire = localdict.get("pl", None)
            except:
                pass

    @api.one
    @api.depends('stock_reel', 'stock_securite', 'stock_reserve')
    def _get_stock_disponible(self):
        self.stock_disponible = self.stock_reel - self.stock_securite - self.stock_reserve

    @api.one
    @api.depends('stock_reel', 'stock_reserve')
    def _get_stock_non_reserve(self):
        self.stock_non_reserve = self.stock_reel - self.stock_reserve


    _name = 'production.article'
    _rec_name = 'code_article'

#   activite_id = fields.Many2one('production.activite', 'Activité', ondelete='cascade')
    code_article = fields.Char('Code article', required=True)
    designation = fields.Char('Désignation', required=True)
    famille_id = fields.Many2one('production.famille', 'Famille', required=True, ondelete='RESTRICT')
    unite = fields.Selection([('u','U'),
                              ('kg','Kg'),
                              ('m2','m²'),
                              ('m','m')], 'Unité par Default', required=True)
    diametre = fields.Float('Diamètre (mm)')
    transversal_ha = fields.Selection([('ha','HA')], 'Transversal HA')
    diametre_longitudinal = fields.Float('Diamètre longitudinal (mm)')
    longitudinal_ha = fields.Selection([('ha','HA')], 'Longitudinal HA')
    maille_transversal = fields.Float('Maille transversale')
    maille_longitudinal = fields.Float('Maille longitudinale')
    largeur = fields.Float('Largeur  (mm)')
    longueur = fields.Float('Longueur (mm)')
    poids_unit = fields.Float('Poids unitaire (Kg/U)', digits=(16,3))
    dimension_m2 = fields.Float('Surface m² (m²/U)', digits=(16,3))
    poids_lineaire = fields.Float('Poids linéaire (Kg/m)', digits=(16,3))
    article_machine_ids = fields.One2many('article.machine.rel', 'article_id', 'Machines')
    nbr_Barres_longitudinales = fields.Float('Nombre de barres longitudinales')
    nbr_Barres_transversales = fields.Float('Nombre de barres transversales')

    #stock
    # reel = disponible + securite + reserve
    # reel = reserve + non_reserve
    stock_reel = fields.Float('Stock réel', readonly="1")
    stock_securite = fields.Float('Stock sécurité')
    #stock_disponible = stock_reel - stock_securite - stock_reserve
    stock_disponible = fields.Float(compute='_get_stock_disponible', string='Stock disponible', readonly="1")
    stock_reserve = fields.Float('Stock réservé', readonly="1")
    #stock_non_reserve = stock_reel - stock_reserve
    stock_non_reserve = fields.Float(compute='_get_stock_non_reserve', string='Stock non réservé', readonly="1")
    stock_minimale = fields.Float('Stock minimale')
    stock_maximale = fields.Float('Stock maximale')

    famille_diametre = fields.Boolean(string='Diamètre', related='famille_id.diametre')
    famille_transversal_ha = fields.Boolean(string='Transversal HA', related='famille_id.transversal_ha')
    famille_diametre_longitudinal = fields.Boolean(string='Diamètre longitudinal', related='famille_id.diametre_longitudinal')
    famille_longitudinal_ha = fields.Boolean(string='Longitudinal HA', related='famille_id.longitudinal_ha')
    famille_maille_transversal = fields.Boolean(string='Maille transversale', related='famille_id.maille_transversal')
    famille_maille_longitudinal = fields.Boolean(string='Maille longitudinale', related='famille_id.maille_longitudinal')
    famille_largeur = fields.Boolean(string='Largeur', related='famille_id.largeur')
    famille_longueur = fields.Boolean(string='Longueur', related='famille_id.longueur')
    famille_poids_unit = fields.Boolean(string='Poids unitaire', related='famille_id.poids_unit')
    famille_dimension_m2 = fields.Boolean(string='Dimension m²', related='famille_id.dimension_m2')
    famille_poids_lineaire = fields.Boolean(string='Poids linéaire', related='famille_id.poids_lineaire')

    @api.model
    def create(self, values):
      
        #calcule nbr_Barres_longitudinales
        nbr_Barres_longitudinales = values.get('nbr_Barres_longitudinales', False)
        if nbr_Barres_longitudinales == False:
            largeur = values.get('largeur', False)
            maille_transversal = values.get('maille_transversal', False)
            if largeur and largeur > 0 and maille_transversal and maille_transversal > 0 :
                values['nbr_Barres_longitudinales'] = int(largeur / maille_transversal)

        #calcule nbr_Barres_transversales
        nbr_Barres_transversales = values.get('nbr_Barres_transversales', False)
        if nbr_Barres_transversales == False:
            longueur = values.get('longueur', False)
            maille_longitudinal = values.get('maille_longitudinal', False)
            if longueur and longueur > 0 and maille_longitudinal and maille_longitudinal > 0 :
                values['nbr_Barres_transversales'] = int(longueur / maille_longitudinal)

        #test code_article doit etre unique
        if self.env['production.article'].search_count([('code_article', '=', values['code_article'])]) > 0:
            raise Warning(_('Erreur!'), 
                        _('Code article existe déjà [ %s ].')% (values['code_article']))

        new_id = super(production_article, self).create(values)
        for obj in self.browse(new_id.id):
            localdict = {
                    'd' : obj.diametre,
                    'dl' : obj.diametre_longitudinal,
                    'mt' : obj.maille_transversal,
                    'ml' : obj.maille_longitudinal,
                    'la' : obj.largeur,
                    'lo' : obj.longueur,
                    'nl' : obj.nbr_Barres_longitudinales,
                    'nt' : obj.nbr_Barres_transversales,
                    'pu' : obj.poids_unit,
                    'mc' : obj.dimension_m2,
                    'pl' : obj.poids_lineaire,
                    }

            #calcul du Poids unit 
            if obj.poids_unit == 0 and obj.famille_id.calcul_poids:
                try:
                    localdict['pu'] = eval(obj.famille_id.calcul_poids, localdict)
                    obj.poids_unit = localdict.get("pu", 0)
                except:
                    pass

            #calcul du Dimension m² 
            if obj.dimension_m2 == 0 and obj.famille_id.calcul_surface:
                try:
                    localdict['mc'] = eval(obj.famille_id.calcul_surface, localdict)
                    obj.dimension_m2 = localdict.get("mc", 0)
                except:
                    pass

            #calcul du Poids linéaire
            if obj.poids_lineaire == 0 and obj.famille_id.calcul_poids_lineaire:
                try:
                    localdict['pl'] = eval(obj.famille_id.calcul_poids_lineaire, localdict)
                    obj.poids_lineaire = localdict.get("pl", 0)
                except:
                    pass

        return new_id

    @api.multi
    def write(self, values):
        obj_id=super(production_article, self).write(values)
        #test code_article doit etre unique
        if self.env['production.article'].search_count([('code_article', '=', self.code_article)]) > 1:
            raise Warning(_('Erreur!'), 
                        _('Code article existe déjà [ %s ].')% (self.code_article))

        return obj_id

    def notif(self, title, record_name, res_id, model, recepteur):
        mail_vals = {
                    'body': '<html>'+title+'</html>',
                    'record_name': record_name,
                    'res_id': res_id,
                    'reply_to': self.env['res.users'].browse(self.env.uid).name,
                    'author_id': self.env['res.users'].browse(self.env.uid).partner_id.id,
                    'model': model,
                    'type': 'email',
                    'email_from': self.env['res.users'].browse(self.env.uid).name,
                    'starred': True,
                    }
        message = self.env['mail.message'].create(mail_vals)

        mail_notif_vals = {
                        'partner_id': self.env['res.users'].browse(recepteur).partner_id.id,
                        'message_id': message.id,
                        'is_read': False,
                        'starred': True,
                        }
        self.env['mail.notification'].create(mail_notif_vals)

        return True

    def verifier_stock(self):
        #list des utilisateur du group production
        group = self.env['res.groups'].search([('name', '=', 'Groupe production')])
        for record in self:
            if record.stock_reel <= record.stock_minimale:
                for user in group.users:
                    title = 'Quantité stock minimale dépassé'
                    record_name = record.code_article
                    res_id = record.id
                    model = 'production.article'
                    recepteur = user.id
                    self.notif(title, record_name, res_id, model, recepteur)
            if record.stock_reel >= record.stock_maximale and record.stock_maximale != 0:
                for user in group.users:
                    title = 'Quantité stock maximale dépassé'
                    record_name = record.code_article
                    res_id = record.id
                    model = 'production.article'
                    recepteur = user.id
                    self.notif(title, record_name, res_id, model, recepteur)
        return True

    @api.multi
    def creer_of(self):

        return { 
            'name': _("Ordre fabrication"),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'production.ordre.fabrication',
            'view_id': False,
            'context': {
                    'default_article_sortant': self.id,
                    },
            }

#   @api.multi
#   def creer_commande(self):
#       return { 
#               'name': _("Commande fournisseur"),
#               'type': 'ir.actions.act_window',
#               'view_type': 'form',
#               'view_mode': 'form',
#               'res_model': 'article.commande.fournisseur.rel',
#               'view_id': False,
#               'context': { 
#                           'default_article_id': self.id,
#                           },
#               }

    @api.multi
    def creer_demande(self):
        return { 
                'name': _("demande d'achat"),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'demande.achat',
                'view_id': False,
                'context': { 
                            'default_article_id': self.id,
                            },
                }