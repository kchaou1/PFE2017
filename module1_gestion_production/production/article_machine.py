# -*- coding: utf-8 -*-

import sys
import openerp
from openerp import models, fields, api, _
from openerp import tools
from datetime import date
from datetime import datetime
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import re
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.modules.module import get_module_resource
import xmlrpclib
import time
from time import mktime
import timestring
import ast


class article_machine_rel(models.Model):

    @api.onchange('famille_id')
    def onchange_famille(self):
        #filter sur les machines selon la famille selectionné
        self.machine_id = []
        res = {}
        ids = []
        if self.famille_id:
            self.env.cr.execute('SELECT id FROM production_machine where %s IN \
                (famille_sortie1, famille_sortie2, famille_sortie3, famille_sortie4) ', (self.famille_id,))
            machine_objs = self.env.cr.fetchall()
            for m in machine_objs:
                ids.append(m)
        else:
            self.env.cr.execute('SELECT id FROM production_machine')
            machine_objs = self.env.cr.fetchall()
            for m in machine_objs:
                ids.append(m)
        res['domain'] = {'machine_id': [('id', 'in', ids)]}
        return res

    _name = "article.machine.rel"

    famille_id = fields.Integer('Famille')#champ pour filtrer les machines
    article_id = fields.Many2one('production.article', 'Article', ondelete='cascade')
    machine_id = fields.Many2one('production.machine', 'Machine', ondelete='cascade')
    cadence = fields.Float('Cadence (Unité/Heure)')

