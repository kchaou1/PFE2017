<b>PFE 2017

<b>Sujet:Optimisation d’un système de gestion de production et de maintenance

<b>Modules :Ce projet est divisé en 2 modules :

<b>1)module 1: gestion de production et de maintenance

https://github.com/aniskchaou/gestion_de_production_et_de_maintenance_PFE2017/tree/master/module1_gestion_production

<b>2)module 2 :module iot (compléméntaire)

https://github.com/aniskchaou/gestion_de_production_et_de_maintenance_PFE2017/tree/master/module_2_iot

<b>Description :Le groupe ADDIXO a proposé ce projet pour pouvoir développer le système de gestion de production et de maintenance du groupe ACIA-SUD, suites aux problèmes rencontrés et la disconcordance entre les différents modules de production.

<b>L’objectif de ce travail étais d’améliorer le système de gestion existant en le rendant plus dynamique et plus flexible en utilisant le concept de l’Industrie 4.0 qui se base sur l’interconnexion des différents postes et acteurs de production par la technologie de l’Internet des objets.

<b>Ce travail a permis de moderniser le système de gestion à tous les niveaux en proposant de nouveaux outils de communication aboutissant à l’amélioration des performances de production et de maintenance en temps réel.

<b>Rapport :https://github.com/aniskchaou/gestion_de_production_et_de_maintenance_PFE2017/blob/master/rapportPFE2017.pdf


<b>Technologies utilisés :python, odoo ,Redis,Java,Kafka,ionic,sailsJS,javascript,mongoDB,Hbase,postgres,XmlRPC

<b>Date :15/01/2017 ->15/06/2017

<b>Siège:ADDIXO
