import time
from threading import Thread
import json
import redis
import xmlrpclib
import ast
import datetime
name="anis"
def timer(name):
        
        
        r = redis.StrictRedis(host='localhost', port=6379, db=0)
        p = r.pubsub(ignore_subscribe_messages=True)
        p.subscribe('bon_of')
        i=0
        
        #attendre les messages dans redis
        for message in p.listen():

             
             if message:
                print '************************ read message  nb_article ********************'
                print message
                
                i=0
                username = 'admin' #the user
                pwd = 'admin'      #the password of the user
                dbname = 'odoo'    #the database
                
                
                
                #recuprer le message et faire le traitement
                msg=message['data']
                sock_common = xmlrpclib.ServerProxy ('http://localhost:8069/xmlrpc/common')
                uid = sock_common.login(dbname, username, pwd)
                sock = xmlrpclib.ServerProxy('http://localhost:8069/xmlrpc/object')
                listes= msg.split(',', 2)
                
                #recherche machine pou determiner idmachine
                args_machine = [('code_machine', '=', listes[0])] #query clause
                ids_machine = sock.execute(dbname, uid, pwd, 'production.machine', 'search', args_machine)
                
                #si la machine recu par redis existe
                if ids_machine:
                   
                   #recherche que  of demarre
                   args = [('state', '=', 'demarre')] 
                   ids = sock.execute(dbname, uid, pwd, 'production.ordre.fabrication', 'search', args)                
                   fields = ['code_machine','code_of','id']
                   resultss = sock.execute(dbname, uid, pwd, 'production.ordre.fabrication', 'read', ids, fields)
                   for x in resultss:
                       
                       #si la machine existe dans of 
                       if x['code_machine'][0]==ids_machine[0]:
                          
                          dt=datetime.datetime.now()
                          bn_of = {
                          'code_of': x['id'],
                          'quantite': int(listes[1])+1,
                          'duree': 66,
                          'date_bon': dt.strftime('%m/%d/%Y'),
                          'unite': 'u',
   
                           }
                          r = redis.StrictRedis(host='localhost', port=6379, db=0)

                          #recherche si existe un bon entree  avec le meme of 
                          args_bn_fab = [('code_of', '=',x['id'] )] #query clause
                          ids_bn_fab = sock.execute(dbname, uid, pwd, 'production.bon.entree.fabrication', 'search', args_bn_fab)
                          
                          # si n'existe pas  un bon entree 
                          if not  ids_bn_fab:
                             partner_id = sock.execute(dbname, uid, pwd, 'production.bon.entree.fabrication', 'create', bn_of) 
                          
                          fields = ['qte_restante']
                          results = sock.execute(dbname, uid, pwd, 'production.bon.entree.fabrication', 'read', ids_bn_fab, fields)
                          
                          message={'type':'production','of': x['code_of'],'code_machine':x['code_machine'][0],'panneaux_fabriques': listes[1]}
                          r.publish('aciasud_downlink_update', message)

                          for x in results:
                              
                              #si existe un bon entree la quantite restante doit entre >0
                              if (int(x['qte_restante']) >0) :
                        
                                 partner_id = sock.execute(dbname, uid, pwd, 'production.bon.entree.fabrication', 'create', bn_of)

#lancer thread backgroud
background_thread = Thread(target=timer, args=(name,))
background_thread.start()   