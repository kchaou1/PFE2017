from openerp.osv import fields,osv
from openerp import models,api
import xmlrpclib


#configuration pour chaque article registre:valeur
class optionmachine_optionmachine(models.Model):
     _name='option.machine'
     _columns={
         'registre' : fields.char('Registre',size=64,required=True),
         'valeur' : fields.char('Valeur',size=64,required=True),
         'option_id':fields.many2one('optionordrecommande.optionordrecommande','Optioncommande'),
     }
optionmachine_optionmachine()
