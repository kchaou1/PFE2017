
{
    'name': 'gestionproduction',
    'version': '1.0',
    'category': '',
    'description': 'gestionproduction Management',
    'author': 'OpenERP SA',
    'depends': ['base','production'],
    'data': ['gestionproduction_view.xml','optionmachine_view.xml','articleparjour_view.xml','maintenance_view.xml','affichage_view.xml','affichage2_view.xml','commande_view.xml','optionordrecommande_view.xml','reglagedate_view.xml'],
    'demo': [],
    'images': [],
    'installable': True,
    # 'application': True,
    'auto_install': False,
}

