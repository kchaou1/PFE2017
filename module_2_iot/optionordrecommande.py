from openerp.osv import fields,osv
from openerp import models,api
import xmlrpclib
import redis



class optionordrecommande_optionordrecommande(models.Model):
     
     #reecriture de la fonction create et diffusion of dans redis
     @api.model
     @api.returns('self', lambda value:value.id)
     def create(self,vals):
        #faire insersion 
        res = super(optionordrecommande_optionordrecommande, self).create(vals)
        
        username = 'admin' #the user
        pwd = 'admin'      #the password of the user
        dbname = 'odoo'
        
        #connexion a redis et subscription   
        r = redis.StrictRedis(host='localhost', port=6379, db=0)
        p = r.pubsub()
        p.subscribe('option_of') 
        sock_common = xmlrpclib.ServerProxy ('http://localhost:8069/xmlrpc/common')
        uid = sock_common.login(dbname, username, pwd)
        strr=""
        sock = xmlrpclib.ServerProxy('http://localhost:8069/xmlrpc/object')
        #print vals
        #recuperer la listes des options
        for x in vals['option'][0][2]:
            #print type(x) ;
            #recherche les configuration registre valeur par id
            args = [('id', '=', x)] #query clause
            ids = sock.execute(dbname, uid, pwd, 'option.machine', 'search', args)
            #print ids;
            fields=['registre','valeur']
            data = sock.execute(dbname, uid, pwd, 'option.machine', 'read', ids,fields)
            
            #faire la concatination de message
            for x in data:
                strr=strr+x['registre']+":"+x['valeur']+","
            
        #pulication message dans redis
        strr=strr[:-1]
        r.publish('option_of', strr)
        return res
     

     _name='optionordrecommande.optionordrecommande'
     _columns={
     'article':fields.many2one('production.article','code_article'),
     'machine':fields.many2one('production.machine','code_machine'),
     'option':fields.many2many('option.machine','optionordrecommande_id','option_id',string='Option'),
     }

optionordrecommande_optionordrecommande()
