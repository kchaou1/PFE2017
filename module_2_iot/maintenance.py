from openerp.osv import fields,osv
from openerp import models,api
import redis
import time
import ast
from kafka import SimpleProducer, SimpleClient
import xmlrpclib


class maintenance_maintenance(models.Model):
     
     _inherit= 'maintenance.demande.intervention'
    
     #recriture de la methode create
     @api.model
     def create(self, vals):

        #insertion dans maintenance
        rec = super(maintenance_maintenance, self).create(vals)
        str1 = str(vals)
        

        #pour kafka
        #client = SimpleClient('localhost:9092')
        #producer = SimpleProducer(client, async=True)
        #producer.send_messages('hello',str1)
        #producer = SimpleProducer(client,async=True,batch_send_every_n=20,batch_send_every_t=60)
        
        #connexion redis
        r = redis.StrictRedis(host='localhost', port=6379, db=0)
        p = r.pubsub()
        p.subscribe('hello')
        
        #diffusion message dans redis
        r.publish('hello', str1)  
        return rec
     
     
     
     

     #recriture de la methode write modifier
     @api.multi
     def write(self,vals):
        #modifier
        res = super(maintenance_maintenance, self).write(vals)
        str1 = str(vals)
        
        #connexion dans redis
        r = redis.StrictRedis(host='localhost', port=6379, db=0)
        p = r.pubsub()
        p.subscribe('hello')
        
        #publication message dans redis
        r.publish('hello', str1)
        return res
maintenance_maintenance()