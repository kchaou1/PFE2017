from openerp.osv import fields,osv
from openerp import models,api
import redis
import time
import ast
from kafka import SimpleProducer, SimpleClient
import xmlrpclib


class commande_commande(models.Model):
     
     _inherit= 'production.commande'
    
     @api.model
     def create(self, vals):

        #creation de commande
        rec = super(commande_commande, self).create(vals) 
        str1 = str(vals)
        
        #pour kafka
        #client = SimpleClient('localhost:9092')
        #producer = SimpleProducer(client, async=True)
        #producer.send_messages('hello',str1)
        #producer = SimpleProducer(client,async=True,batch_send_every_n=20,batch_send_every_t=60)
        
        #connexion redis
        r = redis.StrictRedis(host='localhost', port=6379, db=0)
        p = r.pubsub()
        p.subscribe('hello')
        
        #diffusion message dans redis 
        r.publish('hello', str1)  
        return rec
     

     
     @api.multi
     def write(self,vals):
        
        #modifier commande
        res = super(commande_commande, self).write(vals)
        str1 = str(vals)
        
        #connexion redis
        r = redis.StrictRedis(host='localhost', port=6379, db=0)
        p = r.pubsub()
        p.subscribe('hello')
        
        #diffusion de message
        r.publish('hello', str1) 
        return res 

commande_commande()